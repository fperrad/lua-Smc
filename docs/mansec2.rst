From Model to SMC
-----------------

This section shows a quasi-UML state machine snipet and the equivalent SMC code.
I use the word "quasi" because SMC is not directly derived from UML or Harel state machines.
That means that there are capabilities in UML that are not in SMC and vice versa.
See the SMC FAQ for why this is. LINK

.. toctree::

    mansec2a.rst
    mansec2b.rst
    mansec2c.rst
    mansec2d.rst
    mansec2e.rst
    mansec2f.rst
    mansec2g.rst
    mansec2h.rst
    mansec2i.rst
    mansec2j.rst
    mansec2k.rst
    mansec2l.rst
