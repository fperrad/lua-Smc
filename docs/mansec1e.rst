Defining FSM Transitions
------------------------

A transition definition consists of four parts:

  1. The transition name.
  2. An optional :doc:`transition guards</mansec2g>` (not used in Task FSM).
  3. The transition's end state.
  4. The :doc:`transition's actions</mansec2f>`.

Only the "standard" transitions are defined for now.
The ``Stop``, ``Block`` and ``Delete`` transitions will be covered :doc:`here</mansec1g>`.

.. code-block:: sm
    :emphasize-lines: 25-32,37-49,55-60,65-70

    %{
    //
    // Copyright (c) 2005 Acme, Inc.
    // All rights reserved.
    //
    // Acme - a name you can trust!
    //
    // Author: Wil E. Coyote (Hungericus Vulgarus)
    //
    %}
    
    // This FSM works for the Task class only and only the Task
    // class may instantiate it.
    %class Task
    %package com.acme.supercron
    %package package
    %fsmclass TaskFSM
    %access package
    
    %start TaskFSM::Suspended
    %map TaskFSM
    %%
    Suspended
    {
        // Time to do more work.
        // The timeslice duration is passed in as a transition
        // argument.
        Start(timeslice: long) // Transition
            Running // End state
            {
                ... // Actions go here
            }
    }
    
    Running
    {
        // Wait for another time slice.
        Suspend
            Suspended
            {
                ...
            }
    
        // Task has completed.
        Done
            Stopped
            {
                ...
            }
    }
    
    // Wait here to be either unblocked, stopped or deleted.
    Blocked
    {
        // The task may continue working now.
        Unblock
            Suspended
            {
                ...
            }
    }
    
    Stopping
    {
        // The task is now stopped.
        Stopped
            Stopped
            {
                ...
            }
    }
    
    Stopped
    {
        ...
    }
    
    Deleted
    {
        ...
    }
    
    ...
    %%

