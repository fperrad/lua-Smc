
package Sm

class TestClass {
    private val _fsm: TestClassContext = TestClassContext(this)
    var myProp: Boolean = false

    // Uncomment to see debug output.
    // _fsm.setDebugFlag(true)

    fun getFSM() =_fsm

    fun NoArg() {
        println("No arg")
    }

    fun Output(str: String) {
        println(str)
    }

    fun Output_n(str1: String, n: Int, str2: String) {
        print(str1)
        print(n)
        println(str2)
    }

    fun isOk() = true

    fun isNok() = false

    fun Evt_1() {
        _fsm.Evt_1()
    }

    fun Evt_2() {
        _fsm.Evt_2()
    }

    fun Evt_3() {
        _fsm.Evt_3()
    }

    fun Evt1(n: Int) {
        _fsm.Evt1(n)
    }

    fun Evt2(n: Int) {
        _fsm.Evt2(n)
    }

    fun Evt3(n: Int) {
        _fsm.Evt3(n)
    }
}
