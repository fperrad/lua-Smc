Java (Scala, and Groovy) Sample
-------------------------------

SMC uses Java Beans package for state change notification.
``statemap.FSMContext`` defines the ``addStateChangeListener``
and ``removeStateChangeListener`` methods.
Because the SMC-generated ``FSMContext`` subclass is privately contained
within an application class, it will be necessary to expose the add,
remove methods by adding these methods to the application class.

.. note::
    
    Because applications use multiple state machines
    and a state change handler may register with multiple FSMs,
    I have added the methods ``FSMContext.getName()`` and ``FSMContext.setName(String)``.
    This name should be set and used to distinguish between FSMs.

.. code-block:: java
    
    import java.beans.PropertyChangeListener;
    
    public class AppClass
    {
    //---------------------------------------------------------------
    // Member methods.
    //
        ...
        
        public void addStateChangeListener(PropertyChangeListener listener)
        {
            _fsm.addStateChangeListener(listener);
            return;
        }
        
        public void removeStateChangeListener(PropertyChangeListener listener)
        {
            _fsm.removeStateChangeListener(listener);
            return;
        }
        
    //---------------------------------------------------------------
    // Member data.
    //
        ...
        
        private final AppClassContext _fsm;
    }
    // end of class AppClass

Implement the ``java.beans.PropertyChangeListener`` interface
and pass that implementation to ``AppClass.addStateChangeListener``.
When the ``AppClass`` finite state machine changes state,
the listener receives a ``java.beans.PropertyChangeEvent`` containing:

  - the ``FSMContext`` instance as the source.
  - the "State" property name.
  - the previous state (class ``statemap.State``).
  - the new state (class ``statemap.State``).

.. code-block:: java
    
    import java.beans.PropertyChangeEvent;
    import java.beans.PropertyChangeListener;
    import statemap.FSMContext;
    import statemap.State;
    
    public class StateChangeListener
        implements PropertyChangeListener
    {
    //---------------------------------------------------------------
    // Member methods.
    //
        ...
        
        public void propertyChange(PropertyChangeEvent event)
        {
            FSMContext fsm = (FSMContext) event.getSource();
            String propertyName = event.getPropertyName();
            State previousStatus = (State) event.getOldValue();
            State newState = (State) event.getNewValue();
            
            // Handle the state change event.
            System.out.println(
                "FSM " + fsm.getName() + " went from " + previousState + " to " + newState + ".");
            
            return;
        }
    }
    // end of class StateChangeListener

Register the StateChangeListener with the FSM as follows:

.. code-block:: java
    
    StateChangeListener listener = new StateChangeListener();
    AppClass appInstance = new AppClass();
    
    appInstance.addStateChangeListener(listener);

.. note::
    
    Groovy and Scala also use Java Bean event notification.
