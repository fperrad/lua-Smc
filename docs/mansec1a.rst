Your Task Class
---------------

SMC generates finite state machines for objects - not processes or applications
but for an individual object.
If you have objects that receive asynchronous callbacks
and how objects respond to those callbacks are based on the object state,
then SMC provides a powerful solution.

(Note: this example is based on simple Java and is easily translated to other languages.
Also, this example assumes that you know object-oriented programming and finite state machines (FSMs).)

In this example you are developing a Task object as part of your SuperCron! product:
(Note: The ellipsis (``...``) code will be filled in as we go along.)

.. code-block:: java

    package com.acme.supercron;
    
    public final class Task
        implements TaskEventListener,
                   TimerEventListener 
    {
        public Task() {
            // Object initialization.
            ...
        }
        
        //-----------------------------------------------------------
        // TaskEventListener Interface Implemenation.
        //
        
        // Time for the incomplete task to continue its work for the
        // specified time slice.
        public void start(long timeSlice)
        {
            ...
        }
        
        // Called when a running, incomplete task should suspend
        // running even though its time slice is not expired.
        // Note: the task's running is also suspended when the time
        // slice expires.
        public void suspend()
        {
            ...
        }
        
        // Called when an incomplete task is blocked. Blocked tasks
        // are able to continue running when unblocked.
        public void block()
        {
            ...
        }
        
        // Called when a blocked task is unblocked and allowed
        // to continue running.
        public void unblock()
        {
            ...
        }
        
        // Called when an incomplete task is permanently stopped.
        // Stopped tasks are then deleted.
        public void stop()
        {
            ...
        }
        
        // Called when the task is deleted. Tasks are deleted when
        // either 1) the task has completed running and is now
        // stopped or 2) when the system is shutting down and all
        // are to terminate immediately.
        public void delete()
        {
            ...
        }
        
        //
        // end of TaskEventListener Interface Implemenation.
        //-----------------------------------------------------------
        
        //-----------------------------------------------------------
        // TimerEventListener Interface Implementation.
        //
        
        // Called when the time slice timer expires. If running,
        // the task is suspended.
        public void handleTimeout(TimerEvent event)
        {
            ...
        }
        
        //
        // end of TimerEventListener Interface Implementation.
        //-----------------------------------------------------------
        
        // Remainder of class definition.
        ...
    }

How the ``Task`` class should respond to the ``start``, ``suspend``, etc. method calls
depends on what the ``Task`` is currently doing - that is, it depends on the ``Task``'s state.
