Get the Picture
---------------

The SMC option **-graph** generates  `Graphviz <http://www.graphviz.org/>`_ DOT files using three detail levels:

  - **-glevel 0**: Generates the *least* detail:
    
      + state names,
      + transition names and
      + pop transition "nodes" only.

  - **-glevel 1**: Generates all of the above plus:
    
      + entry and exit actions,
      + transition guards,
      + transition actions,

  - **-glevel 2**: Generates all of the above plus:
    
      + entry and exit action arguments,
      + transition parameters,
      + pop transition arguments,
      + transition action arguments.

SMC generates a rudimentary DOT file - SMC makes no attempt
to produce a "pretty" graph because beauty is in the eye of the beholder.
The default Graphviz settings are used.
You can then modify the DOT file to your heart's content.

.. note::

    Be careful modifying the SMC-generated DOT file directly.
    If you have SMC generate a new DOT file, it will overwrite your work.

Stroll through the SMC Picture Gallery to see examples of SMC-generated DOT files LINK
and what can be done with them.
