JavaScript
----------

**Assumptions:**

  - You have an JavaScript class named ``NetworkIF`` defined in the file ``NetworkIF.js``.
  - The state machine is in ``NetworkIF.sm``.
  - You have successfully :doc:`compiled</mansec4>` ``NetworkIF.sm``.

**Adding the state machine to NetworkIF requires the following changes to NetworkIF.js:**

  1. In the NetworkIF constructor add the line:
     
     .. code-block:: js
     
         this._fsm = new NetworkIF_sm(this);
     
  2. If the start state has entry actions which must be executed prior
     to issuing any transitions add the following code *outside* the NetworkIF constructors:
     
     .. code-block:: js
     
         this._fsm.enterStartState();
     
  3. Whenever you want to issue a transition all you need to do is:
     
     .. code-block:: js
     
         this._fsm.<transition>();
     
     where <transition> is the name of the transition you want to take.
     If the transition takes arguments then pass them in the transition call:
     
     .. code-block:: js
     
         this._fsm.Connect("192.168.3.100", 80);
