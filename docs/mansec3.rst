Adding a state machine to your class
------------------------------------

The SMC-generated code is designed to be loosely coupled with your application software.
The only changes that you need to make to your code is to:

  1. Include the SMC class definitions into your application (stored in smc/lib by programming language name):
      - **C**: Have ``lib/C/statemap.h`` in the include path.
      - **C++**: Have ``lib/C++/statemap.h`` in the include path.
      - **C#**: Have ``lib/DotNet/statemap.dll`` included in the Visual Studio studio.
      - **Groovy**: Have ``lib/Groovy/statemap.jar`` in the classpath.
      - **Java**: Have ``lib/Java/statemap.jar`` in the classpath.
      - **JavaScript**: Have ``lib/JavaScript/statemap.js`` accessible to ``<script>`` tag.
      - **Lua**: Have the ``lib/Lua/statemap.lua`` module on your Lua ``package.path``
        (initialized by the environment variable ``LUA_PATH``).
      - **Objective-C**: Have ``lib/ObjC/statemap.h`` in the include path.
      - **Perl**: Have ``StateMachine::Statemap`` module on your Perl library path ``@INC``.
      - **PHP**: Have ``StateMachine/statemap.php`` in your PHP include_path.
      - **Python**: Have the statemap module on your import source path ``sys.path``.
      - **Ruby**: Have the statemap module on your Ruby library path ``$LOAD_PATH``.
      - **Scala**: Have ``lib/Scala/statemap.jar`` in the classpath.
      - **Tcl**: Have the ``lib/Tcl/statemap1.0`` package on your path.
      - **VB.net**: Have ``lib/DotNet/statemap.dll`` included in the Visual Studio studio.

  2. Include the state machine's source file:
      - **C**: ``#include "AppClass_sm.h"``
      - **C++**: ``#include "AppClass_sm.h"``
      - **C#**: put the ``AppClass_sm.vb`` file in your Visual Studio solution.
      - **Groovy**: If ``AppClassContext`` class is in the same package as ``AppClass``,
        no importation is needed.
      - **Java**: If ``AppClassContext`` class is in the same package as ``AppClass``,
        no importation is needed.
      - **JavaScript**: Have ``AppClass_sm.js`` file accessible to ``<script>`` tag.
      - **Lua**: ``require 'AppClass_sm'``
      - **Objective-C**: ``#import "AppClass_sm.h"``
      - **Perl**: ``use AppClass_sm;``
      - **PHP**: ``require_once 'AppClass_sm';``
      - **Python**: ``import AppClass_sm``
      - **Ruby**: ``require 'AppClass_sm'``
      - **Scala**: If ``AppClassContext`` class is in the same package as ``AppClass``,
        no importation is needed.
      - **Tcl**: ``source ./AppClass_sm.tcl``
      - **VB.net**: put the AppClass_sm.vb file in your Visual Studio solution.
    
    where AppClass is your application class with an associated SMC state machine.
    
  3. Instantiate the state machine's context object.
  4. If you want to execute the start state's entry actions call
     the state machine context's ``enterStartState`` method.
     This is *not* needed to set the start state as that is done
     when the state machine context is instantiated.
     ``enterStartState`` only executes the start state's entry actions (if any exist).

That's all you need to do. Whenever you want to issue a transition,
call the context's object transition method.

SMC does not change your code or require you to change your code's logic.
SMC does not require that your class inherit or implement any SMC class.
SMC does not use state transition arrays or switch statements.
SMC state machines are easy to add with minimal impact to your existing code.

.. warning::
    
    THIS BEARS REPEATING. Do **NOT** issue a transition from within an action -
    it will cause the state machine to throw an exception
    since actions are not allowed to issue a transitions.

For an explanation of why this is, see the SMC FAQ question: Why can't an action issue a transition?

If you do need to issue a transition from an action
see the :doc:`section 7, "Queuing Up"</mansec7>` explaining how to use a "transition queue".

.. toctree::

    mansec3_c.rst
    mansec3_cpp.rst
    mansec3_objc.rst
    mansec3_java.rst
    mansec3_tcl.rst
    mansec3_vb.rst
    mansec3_cs.rst
    mansec3_groovy.rst
    mansec3_lua.rst
    mansec3_py.rst
    mansec3_php.rst
    mansec3_pl.rst
    mansec3_rb.rst
    mansec3_scala.rst
    mansec3_js.rst
