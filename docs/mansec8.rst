Packages & Namespaces
---------------------

Putting the SMC-generated classes into a specific Java package/C++
namespace/Tcl namespace/Groovy package/Perl package/Ruby module/Scala
package is as easy placing the ``%package`` keyword at the
:doc:`top of your .sm file</mansec1a>` along with the ``%class`` keyword.

(Aside: Remember to place ``namespace import <::namespace::*>``
after sourcing in a ``*_sm.tcl`` file.)

To import a C++ namespace, Java class, Tcl package, VB.net, C# namespace
, Groovy class, Lua module, Python module, Perl module, PHP file, Ruby module
or Scala class, use the ``%import`` keyword:

.. code-block:: sm

    %import <name>

.. note::

    There is *no* semicolon at the line's end.

The ``%import`` is translated into the following syntax:

  - **C++**:    ``using namespace <name>;``
  - **Java**:   ``import <name>;``
  - **Tcl**:    ``package require <name>``
  - **VB.net**: ``Imports <name>``
  - **C#**:     ``using <name>``
  - **Groovy**: ``import <name>``
  - **Lua**:    ``require '<name>'``
  - **Python**: ``import <name>``
  - **Perl**:   ``use <name>;``
  - **PHP**:    ``require_once '<name>';``
  - **Ruby**:   ``require '<name>'``
  - **Scala**:  ``import <name>``

See examples/Java/EX4, C++/EX4, Tcl/EX4, VB/EX4, CSharp/EX4, Perl/EX4
and Ruby/EX4 for sample code which uses packages/namespaces with SMC.

.. rubric:: Fully Qualified Class Names

If you are not able to place the SMC-generated classes into
the same package/namespace as the ``%class`` context class,
then you will need to specify the context class' fully qualified name.
The SMC ``%class`` key supports Java, C++ and Tcl fully qualified name syntax.
For example, the following are accepted:

.. code-block:: sm

    // For a Java, VB.net and C# application
    %class com.acme.project.AppClass

and

.. code-block:: sm

    // For a C++ or Tcl application
    %class ::acme::project::AppClass
