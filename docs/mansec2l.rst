Default Transitions
-------------------

What happens if a state receives a transition that is not defined in that state?
SMC has two separate mechanisms for handling that situation.

The first is the "Default" state.
Every ``%map`` may have a special state named "Default" (the uppercase D is significant).
Like all other states, the Default state contains transitions.

.. code-block:: sm

    Default
    {
        // Valid run request but transition occurred in an invalid
        // state. Send a reject reply to valid messages.
        Run(msg: const Message&)
          [ctxt.isProcessorAvailable() == true &&
           msg.isValid() == true]
            nil
            {
                RejectRequest(msg);
            }

        // Ignore invalid messages are ignored when received in
        // an invalid state.
        Run(msg: const Message&)
            nil
            {}
        
        Shutdown
            ShuttingDown
            {
                StartShutdown();
            }
    }

Default state transitions may have guards and arguments features as non-default transitions.
This means the Default state may contain multiple guarded and one unguarded definition for the same transition.

The second mechanism is the "Default" transition.
This is placed inside a state and is used to back up all transitions.

.. code-block:: sm

    Connecting
    {
        // We are now connected to the far-end. Now we can logon.
        Connected
            Connected
            {
                logon();
            }
    
        // Any other transition at this point is an error.
        // Stop the connecting process and retry later.
        Default
            RetryConnection
            {
                stopConnecting();
            }
    }

Because any transition can fall through to the Default transition, Default transitions:

  - May *not* have an argument list.
  - A Default transition may take a guard.
  - Putting a Default transition in the Default state means 
    that all transitions will be handled - it is the transition definition of last resort.

.. rubric:: Transition Precedence

Transition definitions have the following precedence:

  1. Guarded transition
  2. Unguarded transition
  3. The Default state's guarded definition.
  4. The Default state's unguarded definition.
  5. The current state's guarded Default transition.
  6. The current state's unguarded Default transition.
  7. The Default state's guarded Default transition.
  8. The Default state's unguarded Default transition.

.. warning::

    Since SMC does not force you to specify a Default state or a Default transition,
    it is possible that there is no transition defined.
    If SMC falls through this list, it will throw a "Transition Undefined" exception.
