#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin";

use Test::More;
#use Test::LongString;
use Util;

#$Util::smc = 'java -jar Smc.jar';
#$Util::test_graph = 0;
#$Util::test_table = 0;
#@Util::tests = ( 'Simple' );

my @opt = (
    '',
    '-g0',
    '-g1',
    '-noc',
    '-noc -g0',
    '-noc -g1',
    '-reflect',
    '-serial',
);

$Util::config = {
    code        => '// comment',
    import1     => 'java.io.File',
    import2     => 'kotlin.io.*',
    include1    => 'a',
    include2    => 'b',
    is_ok       => 'ctxt.isOk()',
    is_nok      => 'ctxt.isNok()',
    false       => 'false',
    n           => 'n',
    param       => 'n: Int',
    prop1       => 'myProp = true;',
    prop2       => 'myProp = false;',
};

my %re = (
    TransUndef  => 'statemap\.TransitionUndefinedException: State: (Sm::)?Map_1(::|\.)State_1, Transition: Evt_1',
);

sub test_smc_kotlin {
    my ($test, $options) = @_;
    unlink("t/kotlin/Sm/TestClass.sm");
    unlink("t/kotlin/Sm/TestClassContext.kt");
    Util::do_fsm('kotlin/Sm', $test);
    system("${Util::smc} -kotlin ${options} t/kotlin/Sm/TestClass.sm");
    my $out = Util::run("cd t/kotlin && kotlinc ../../runtime/kotlin/statemap.kt Sm/TestClass.kt Sm/TestClassContext.kt ${test}.kt -include-runtime -d ${test}.jar");
    diag($out) if $out;
    my $trace = $options =~ /-g0/ && ${Util::smc} !~ /\.jar/ ? 'g0' : '';
    $out = Util::run('java -jar', "t/kotlin/$test.jar", $trace);
    my $expected = $trace
                 ? Util::slurp("t/templates/${test}.g0.out")
                 : Util::slurp("t/templates/${test}.out");
    if ($expected =~ /^like/) {
        like($out, qr{$re{$test}}, "$test $options");
    }
    else {
        $out =~ s/n: Int/n/gm;
        is($out, $expected, "$test $options");
    }
}

unless (`kotlinc -version 2>&1` =~ /kotlin/i) {
    plan skip_all => 'no kotlin';
}
plan tests => scalar(@Util::tests) * scalar(@opt);
diag($Util::smc);

for my $test (@Util::tests) {
    Util::test_smc_with_options('kotlin/Sm', \&test_smc_kotlin, $test, \@opt);
}

