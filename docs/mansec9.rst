Be Persistant!
--------------

This section describes how to persist an SMC-generated finite state machine
and restore it at a later date.
In the following examples I persist the FSM to a flat file
but they can be modified to work with other storage types.
The focus is only capturing the FSM's current state and state stack.
There is no other data to persist in the SMC-generated code.

The examples use the class ``AppClass`` which
has an associated finite state machine stored in its data member ``_fsm``.


.. note::

    the following sample code requires the .sm file be compiled with the **-serial** option.

.. toctree::

    mansec9_cpp.rst
    mansec9_java.rst
    mansec9_tcl.rst
    mansec9_vb.rst
    mansec9_cs.rst
