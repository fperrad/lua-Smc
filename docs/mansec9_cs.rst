C#
--

SMC makes full use of .net's object serialization.
Assuming that ``AppClass`` has the ``[Serializable]`` attribute
and that the ``_fsm`` member data is not marked as ``[NonSerialized]``,
then serializing the ``AppClass`` instance will also serialize the FSM:

SMC considers the code it generates to be subservient to the application code.
For this reason the SMC code does not serialize its references
to the FSM context owner or property listeners.
The application code after deserializing the FSM *must* call
the ``Owner`` property setter to re-establish the application/FSM link.
If the application listens for FSM state transitions,
then event handlers must also be put back in place.

.. code-block:: c#

    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    
    [Serializable]
    public class AppClass :
        IDeserializationCallback
    {
        private AppClassContext _fsm;
        
        public AppClass()
        {
            _fsm = new AppClassContext(this);
        }
        
        // Restore the FSM context's reference to this object.
        // This is necessary because AppClass and AppClassContext
        // reference each other. When AppClass is serialized, its
        // AppClassContext instance is serialized. But when
        // AppClassContext is serialized, its AppClass instance is
        // not serialized which breaks the circual reference. When
        // AppClassContext is deserialized, its AppClass reference
        // is null. Hence the need to implement IDeserialization
        // and reset AppClassContext's AppClass reference.
        // Event handlers must also be put back in place for the same
        // reason.
        void IDeserializationCallback.OnDeserialization(Object sender)
        {
            _fsm.Owner = this;
            _fsm.StateChange += handler;
        }
        
        static void Main(string[] args)
        {
            Serialize();
            Deserialize();
        }
        
        static void Serialize()
        {
            AppClass appInstance = new AppClass();
            FileStream fstream =
                new FileStream("fsm_serial.dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();
            
            try
            {
                formatter.Serialize(fstream, appInstance);
            }
            catch (SerializationExceptionserialex)
            {
                // Handle exception.
            }
            finally
            {
                fstream.Close();
            }
        }
        
        ...
    }

Recreating the persisted ``AppClass`` instance is equally simple:

.. code-block:: c#

    static void Deserialize()
    {
        AppClass appInstance = null;
        FileStream fstream =
            new FileStream("fsm_serial.dat", FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        
        try
        {
            appInstance = (AppClass) formatter.Deserialize(fstream);
        }
        catch (SerializationException serialex)
        {
            // Handle exception.
        }
        finally
        {
            fstream.Close();
        }
    }
