Tcl
---

The following code should be added to a class which has an associated FSM
and which will be issuing transitions from a transition action.

(The example code is based on examples/Tcl/EX1.)

  1. **Add a transition queue private member.**
     Place transitions here for later execution.
     One transition queue is needed for each context class instance.
     
     .. code-block:: tcl
         
         private variable _transition_queue;
     
  2. **Add a Transition method.**
     Queues up a transition and issues it only if the FSM is not currently in a transition:
     
     .. code-block:: tcl
         
         private method Transition {transname} {
             lappend _transition_queue $transname;
             
             if {[$_fsm isInTransition] == 0} {
                 while {[llength $_transition_queue] > 0} {
                     set transition [lindex $_transition_queue 0];
                     set _transition_queue [lrange $_transition_queue 1 end];
                     
                     // Issue the transition.
                     $_fsm $transition;
                 }
             }
             return -code ok;
         }
     
  3. **Replace transition calls.** Replace your transition method calls:
     
     ``$_fsm Zero;``
     
     with transition method calls:
     
     ``transition "Zero";``
