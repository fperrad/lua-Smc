Java
----

The following code should be added to a class which has an associated FSM
and which will be issuing transitions from a transition action.

(The example code is based on examples/Java/EX1.)

  1. **Add a transition table private member.**
     This table is a list of all transition methods.
     The table is static because the FSM is common for all instances of the context class.
     
     .. code-block:: java
     
         import java.util.HashMap;
         ...
         private static HashMap _transition_map;
     
  2. **Add a transition queue private member.**
     Place transitions here for later execution.
     One transition queue is needed for each context class instance.
     
     .. code-block:: java
     
         import java.util.LinkedList;
         ...
         private LinkedList _transition_queue;
     
  3. **Find all transition methods.**
     In the class ``static`` block,
     fill in the transition table with the transition methods using Java reflection:
     
     .. code-block:: java
     
         import java.lang.reflect.Method;
         ...
         static
         {
             try
             {
                 Class context = AppClassContext.class;
                 Method[] transitions = context.getDeclaredMethods();
                 String name;
                 int i;
                 
                 for (i = 0; i < transitions.length; ++i)
                 {
                     name = transitions[i].getName();
                     
                     // Ignore the getState and getOwner methods.
                     if (name.compareTo("getState") != 0 && name.compareTo("getOwner") != 0)
                     {
                         _transition_map.put(name, transitions[i]);
                     }
                 }
             }
             catch (Exception ex)
             {}
         }
     
  4. **Add a transition method.**
     Queues up a transition and issues it only if the FSM is not currently in a transition:
     
     .. code-block:: java
     
         private synchronized void transition(String trans_name)
         {
             // Add the transition to the queue.
             _transition_queue.add(trans_name);
             
             // Only if a transition is not in progress should a
             // transition be issued.
             if (_fsm.isInTransition() == false)
             {
                 String name;
                 Method transition;
                 Object[] args = new Object[0];
                 
                 while (_transition_queue.isEmpty() == false)
                 {
                     name = (String) _transition_queue.remove(0);
                     transition = (Method) _transition_map.get(name);
                     try
                     {
                         transition.invoke(_fsm, args);
                     }
                     catch (Exception ex)
                     {
                         // Handle exception.
                     }
                 }
             }
             
             return;
         }
     
  5. **Replace transition calls.**
     Replace your transition method calls:
     
     ``_fsm.Zero();``
     
     with transition method calls:
     
     ``transition("Zero");``
