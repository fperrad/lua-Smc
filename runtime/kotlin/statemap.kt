//
// The contents of this file are subject to the Mozilla Public
// License Version 1.1 (the "License"); you may not use this file
// except in compliance with the License. You may obtain a copy of
// the License at http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS
// IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
// implied. See the License for the specific language governing
// rights and limitations under the License.
//
// The Original Code is State Machine Compiler (SMC).
//
// The Initial Developer of the Original Code is Charles W. Rapp.
// Portions created by Charles W. Rapp are
// Copyright (C) 2000 - 2005 Charles W. Rapp.
// All Rights Reserved.
//
// Port to Kotlin by Francois Perrad, francois.perrad@gadz.org
// Copyright 2017, Francois Perrad.
// All Rights Reserved.
//
// Contributor(s):
//

package statemap

import java.beans.PropertyChangeListener
import java.beans.PropertyChangeSupport
import java.io.PrintStream
import java.io.Serializable
import java.util.Stack

class StateUndefinedException() : Throwable() {
}

class TransitionUndefinedException(reason: String) : Throwable(reason) {
}

abstract class FSMContext<State>(start: State) : Serializable {
    private var _state: State? = start
    private var _stateStack: Stack<State> = Stack<State>()
    protected var _transition: String = ""
    private var _debugFlag: Boolean = false
    private var _debugStream: PrintStream = System.err
    private var _listeners: PropertyChangeSupport = PropertyChangeSupport(this)
    private var _isInTransaction: Boolean = false

    abstract fun enterStartState(): Unit

    fun getDebugFlag(): Boolean = _debugFlag

    fun setDebugFlag(flag: Boolean) {
        _debugFlag = flag
    }

    fun getDebugStream(): PrintStream = _debugStream

    fun setDebugStream(stream: PrintStream) {
        _debugStream = stream
    }

    fun getTransition(): String = _transition

    // Is this state machine in a transition? If state is null,
    // then true; otherwise, false.
    fun isInTransition(): Boolean = _isInTransaction

    fun setState(state: State) {
        val previousState = _state
        if (_debugFlag)
            _debugStream.println("ENTER STATE     : " + state)
        _state = state
        _isInTransaction = false
        // Inform all listeners about this state change
        _listeners.firePropertyChange("State", previousState, _state)
    }

    fun getState(): State {
        if (_isInTransaction)
            throw StateUndefinedException()
        return _state ?: throw StateUndefinedException()
    }

    fun clearState() {
        _isInTransaction = true
    }

    fun pushState(state: State) {
        val previousState = _state
        if (_state == null)
            throw NullPointerException("uninitialized state")
        if (_isInTransaction)
            throw StateUndefinedException()
        if (_debugFlag)
            _debugStream.println("PUSH TO STATE   : " + state)
        _stateStack.push(_state)
        _state = state
        // Inform all listeners about this state change
        _listeners.firePropertyChange("State", previousState, _state)
    }

    fun popState() {
        if (_stateStack.size == 0) {
            if (_debugFlag)
                _debugStream.println("POPPING ON EMPTY STATE STACK.")
            throw NoSuchElementException("empty state stack")
        }
        val previousState = _state
        _state = _stateStack.pop()
        _isInTransaction = false
        if (_debugFlag)
            _debugStream.println("POP TO STATE    : " + _state)
        // Inform all listeners about this state change
        _listeners.firePropertyChange("State", previousState, _state)
    }

    fun emptyStateStack() {
        _stateStack = Stack<State>()
    }

    fun addStateChangeListener(listener: PropertyChangeListener) {
        _listeners.addPropertyChangeListener("State", listener)
    }

    fun removeStateChangeListener(listener: PropertyChangeListener) {
        _listeners.removePropertyChangeListener("State", listener)
    }
}

