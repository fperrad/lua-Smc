A Simple Transition
-------------------

.. image:: ./Images/SimpleTrans.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans    Next State      Actions
        Run         Running         {}
    }


A state and transition names must have the form ``[A-Za-z_][A-Za-z0-9_]*``.
