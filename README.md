
lua-Smc
=======

a port on [lua-Coat](https://fperrad.frama.io/lua-Coat)

Delta vs [SMC](http://smc.sourceforge.net/)
-------------------------------------------

### Install

with [luarocks](http://luarocks.org/)

    $ luarocks install lua-smc

### Compiler Command

in the source tree :

    $ bin/smc

after install :

    $ smc

### Windows binary

a standalone executable for Windows ([LuaJIT](http://luajit.org/) based) is available :
[smc.exe](https://framagit.org/fperrad/lua-Smc/raw/master/src_c/win/smc.exe)

### Command Line Options

- -dump : Ascii output

- -load : Use a language from a plugin

### Language

- %source _filename_ : inclusion directive, allows to split a FSM definition into multiple files

### Target Languages Not Currently Supported

- C#
- Objective C
- Tcl
- VB

### Documentation

ported to [Sphinx](http://sphinx-doc.org/), see <https://fperrad.frama.io/lua-Smc/>.

### Internals

- the code generation uses a template engine ([lua-CodeGen](https://fperrad.frama.io/lua-CodeGen))

### Maintainer Documentation

Many graphics built by :

    $ make -C maintainer

Test Suite
----------

    $ make unit_test

    $ make test # for target languages

(work for lua-Smc & SMC)
