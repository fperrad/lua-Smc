SMC Telephone Demo
------------------

.. note::

    Make sure that you have your computer's sound turned on
    otherwise you will miss the demo's point.

**Instructions on how to use a telephone:**

  - Pick up the receiver.
  - Dial the desired telephone number.
  - Try some of the following telephone numbers:
      - 555-1212
      - 555-9263
      - 1-212-555-1234
      - 879-6877
      - 1-802-521-6448
      - Dial a "#" or a "*"
      - 911
  - When you have completed the call, put the receiver down.
    **If you don't put down the receiver after the call has finished,
    a "receiver left off hook" alarm will sound.**

**Finite State Machine Log**

As you make your telephone calls, watch the FSM log.
This text area displays the FSM's current state,
transitions and what actions are executing
(both transition actions and a state Entry/Exit actions).
This FSM log shows how the telephone is reacting to your inputs.
You can learn how SMC-generated FSMs work by stepping
through the logging messages as you look at the telephone FSMs: CallMap and PhoneNumber.
(The source code is :doc:`Telephone.sm</demosrc>`.)

.. raw:: html

    <div>
        <style type="text/css">
            table.telephone td { text-align: center; }
        </style>
        <table class="telephone" border="0" cols="3" width="450" height="200">
          <tr>
            <td colspan="5"><input type="text" readonly="1" id="display" maxlength="50"/></td>
          </tr>
          <tr>
            <td width="200" colspan="5"><input type="button" id="receiver" value="Pick up receiver" onclick="app.Receiver();"/></td>
          </tr>
          <tr>
            <td></td>
            <td width="40"><input type="button" value="1" onclick="app.Digit('1');"/></td>
            <td width="40"><input type="button" value="2" onclick="app.Digit('2');"/></td>
            <td width="40"><input type="button" value="3" onclick="app.Digit('3');"/></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td width="40"><input type="button" value="4" onclick="app.Digit('4');"/></td>
            <td width="40"><input type="button" value="5" onclick="app.Digit('5');"/></td>
            <td width="40"><input type="button" value="6" onclick="app.Digit('6');"/></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td width="40"><input type="button" value="7" onclick="app.Digit('7');"/></td>
            <td width="40"><input type="button" value="8" onclick="app.Digit('8');"/></td>
            <td width="40"><input type="button" value="9" onclick="app.Digit('9');"/></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td width="40"><input type="button" value="*" onclick="app.Digit('10');"/></td>
            <td width="40"><input type="button" value="0" onclick="app.Digit('0');"/></td>
            <td width="40"><input type="button" value="#" onclick="app.Digit('11');"/></td>
            <td></td>
          </tr>
        </table>
        <p>log</p>
        <textarea id="trace" cols="60" rows="10" readonly="1"></textarea>
        <audio id="audio_0" src="./sounds/0.wav" preload="auto"></audio>
        <audio id="audio_1" src="./sounds/1.wav" preload="auto"></audio>
        <audio id="audio_2" src="./sounds/2.wav" preload="auto"></audio>
        <audio id="audio_3" src="./sounds/3.wav" preload="auto"></audio>
        <audio id="audio_4" src="./sounds/4.wav" preload="auto"></audio>
        <audio id="audio_5" src="./sounds/5.wav" preload="auto"></audio>
        <audio id="audio_6" src="./sounds/6.wav" preload="auto"></audio>
        <audio id="audio_7" src="./sounds/7.wav" preload="auto"></audio>
        <audio id="audio_8" src="./sounds/8.wav" preload="auto"></audio>
        <audio id="audio_9" src="./sounds/9.wav" preload="auto"></audio>
        <audio id="audio_10" src="./sounds/10.wav" preload="auto"></audio>
        <audio id="audio_11" src="./sounds/11.wav" preload="auto"></audio>
        <audio id="audio_12" src="./sounds/12.wav" preload="auto"></audio>
        <audio id="audio_13" src="./sounds/13.wav" preload="auto"></audio>
        <audio id="audio_14" src="./sounds/14.wav" preload="auto"></audio>
        <audio id="audio_15" src="./sounds/15.wav" preload="auto"></audio>
        <audio id="audio_16" src="./sounds/16.wav" preload="auto"></audio>
        <audio id="audio_17" src="./sounds/17.wav" preload="auto"></audio>
        <audio id="audio_18" src="./sounds/18.wav" preload="auto"></audio>
        <audio id="audio_19" src="./sounds/19.wav" preload="auto"></audio>
        <audio id="audio_20" src="./sounds/20.wav" preload="auto"></audio>
        <audio id="audio_30" src="./sounds/30.wav" preload="auto"></audio>
        <audio id="audio_40" src="./sounds/40.wav" preload="auto"></audio>
        <audio id="audio_50" src="./sounds/50.wav" preload="auto"></audio>
        <audio id="audio_50_cents_please" src="./sounds/50_cents_please.wav" preload="auto"></audio>
        <audio id="audio_911" src="./sounds/911.wav" preload="auto"></audio>
        <audio id="audio_AM" src="./sounds/AM.wav" preload="auto"></audio>
        <audio id="audio_PM" src="./sounds/PM.wav" preload="auto"></audio>
        <audio id="audio_and" src="./sounds/and.wav" preload="auto"></audio>
        <audio id="audio_busy" src="./sounds/busy_signal.wav" preload="auto"></audio>
        <audio id="audio_could_not_be_completed" src="./sounds/could_not_be_completed.wav" preload="auto"></audio>
        <audio id="audio_dialtone" src="./sounds/dialtone.wav" preload="auto"></audio>
        <audio id="audio_error_signal" src="./sounds/error_signal.wav" preload="auto"></audio>
        <audio id="audio_exactly" src="./sounds/exactly.wav" preload="auto"></audio>
        <audio id="audio_fast_busy" src="./sounds/fast_busy_signal.wav" preload="auto"></audio>
        <audio id="audio_nyctemp" src="./sounds/nyctemp.wav" preload="auto"></audio>
        <audio id="audio_oclock" src="./sounds/oclock.wav" preload="auto"></audio>
        <audio id="audio_oh" src="./sounds/oh.wav" preload="auto"></audio>
        <audio id="audio_phone_off_hook" src="./sounds/phone_off_hook.wav" preload="auto"></audio>
        <audio id="audio_ringing" src="./sounds/ring.wav" preload="auto"></audio>
        <audio id="audio_second" src="./sounds/second.wav" preload="auto"></audio>
        <audio id="audio_seconds" src="./sounds/seconds.wav" preload="auto"></audio>
        <audio id="audio_the_number_you_have_dialed" src="./sounds/the_number_you_have_dialed.wav" preload="auto"></audio>
        <audio id="audio_the_time_is" src="./sounds/the_time_is.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_0" src="./sounds/touch_tone_0.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_1" src="./sounds/touch_tone_1.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_2" src="./sounds/touch_tone_2.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_3" src="./sounds/touch_tone_3.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_4" src="./sounds/touch_tone_4.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_5" src="./sounds/touch_tone_5.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_6" src="./sounds/touch_tone_6.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_7" src="./sounds/touch_tone_7.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_8" src="./sounds/touch_tone_8.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_9" src="./sounds/touch_tone_9.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_10" src="./sounds/touch_tone_10.wav" preload="auto"></audio>
        <audio id="audio_touch_tone_11" src="./sounds/touch_tone_11.wav" preload="auto"></audio>
        <script src="./statemap.js"></script>
        <script src="./Telephone_sm.js"></script>
        <script src="./Telephone.js"></script>
    </div>
