Entry and Exit Actions
----------------------

.. image:: ./Images/EntryExit.png

.. code-block:: sm

    // State
    Idle
    Entry {StartTimer("Idle", 1); CheckQueue();}
    Exit {StopTimer("Idle");}
    {
        // Transitions
    }

When a transition leaves a state, it executes the state's exit actions before any of the transition actions.
When a transition enters a state, it executes the state's entry actions.
A transition executes actions in this order:

  1. "From" state's exit actions.
  2. Set the current state to null.
  3. The transition actions in the same order as defined in the .sm file.
  4. Set the current state to the "to" state.
  5. "To" state's entry actions.

As of version 6.0.0, SMC generates a ``enterStartState`` method which executes the start state's entry acitons.
It is now up to the application to call the start method after instantiating the finite state machine context.
If it is not appropriate to execute the entry actions upon start up, then do not call ``enterStartState``.
You are not required to call this method to set the finite state machine start state.
That is done when the FSM is instantiated.
This method is used only to execute the start state's entry actions.

If you do call this method, be sure to do it outside of the context class' constructor.
This is because entry actions call context class methods.
If you call ``enterStateState`` from within you context class' constructor,
the context instance will be referenced before it has completed initializing which is a bad thing to do.

**enterStartState does not protect against being called multiple times.**
It should be called at most once and prior to issuing any transitions.
Failure to follow this requirement may result in inappropriate finite state machine behavior.

Whether a state's Entry and Exit actions are executed depends on the type of transition taken.
The following table shows which transitions execute the "from" state's Exit actions
and which transitions execute the "to" state's Entry actions.

============================  ======================  ======================
Transition Type               Execute "From" State's  Execute "To" State's
                              Exit Actions?           Entry Actions?
============================  ======================  ======================
Simple Transition             Yes                     Yes
External Loopback Transition  Yes                     Yes
Internal Loopback Transition  No                      No
Push Transition               No                      Yes
Pop Transition                Yes                     No
============================  ======================  ======================

.. warning::

    Entry and exit actions are *not* supported for the :doc:`Default state</mansec2l>` which is not an actual state.
    See more in the "Default Transitions" section.

.. warning::

    From this point on, SMC diverges from UML.
    SMC uses the idea of multiple machines and pushing and popping states
    as way of breaking complicated behavior up into simpler parts.
    UML acheives much the same by grouping states into superstates.
    They may be equivalent in ability but I find the idea of pushing
    to a new state easier to understand because it is similar to the subroutine call. 