Java
----

SMC makes full use of Java's object serialization.
Assuming that ``AppClass`` declares implements ``java.io.Serializable``
and that the ``_fsm`` member data is not marked as ``transient``,
then by serializing the ``AppClass`` instance will also serialize the FSM.

SMC considers the code it generates to be subservient to the application code.
For this reason the SMC code does not serialize its references
to the FSM context owner or property listeners.
The application code after deserializing the FSM must call
the setOwner method to re-establish the application/FSM link.
If the application listens for FSM state transitions ``addStateChangeListener``
*must* also be called to put the listeners in place.

.. code-block:: java

    public final class AppClass
        implements java.io.Serializable
    {
        private AppClassContext _fsm;
        
        public AppClass()
        {
            _fsm = newAppClassContext();
        }
        
        // Restore the FSM context's reference to this object.
        // This is necessary because AppClass and AppClassContext
        // reference each other. When AppClass is serialized, its
        // AppClassContext instance is serialized. But when
        // AppClassContext is serialized, its AppClass instance is
        // not serialized which breaks the circual reference. When
        // AppClassContext is deserialized, its AppClass reference
        // is null. Hence the need to implement readObject
        // and reset AppClassContext's AppClass reference.
        // The state change property listeners list is empty for
        // the same reason.
        private void readObject(java.io.ObjectInputStream istream)
            throws java.io.IOException,
                   ClassNotFoundException
        {
            // Do the default read first which sets _fsm to null.
            istream.defaultReadObject();
            
            // Now set the FSM's owner.
            _fsm.setOwner(this);
            
            // State change listeners must also be added back.
            _fsm.addStateChangeListener(_stateListener);
            
            return;
        }
        
        public static void main(String[]args)
        {
            Serialize();
            Deserialize();
        }
        
        public static void Serialize()
        {
            AppClass appInstance = new AppClass();
            ObjectOutputStream ostream =
                new ObjectOutputStream(
                    new FileOutputStream("./fsm_serial.bin"));
            
            try
            {
                ostream.writeObject(appInstance);
            }
            catch (java.io.IOException ioex)
            {
                // Handle serialization exception.
            }
            finally
            {
                ostream.close();
            }
        }
        
        ...
    }

Recreating the persisted ``AppClass`` instance is equally simple:

.. code-block:: java

    public static void Deserialize()
    {
        AppClass appInstance = null;
        ObjectInputStream istream =
            new ObjectInputStream(
                new FileInputStream("./fsm_serial.bin"));
        
        try
        {
            appInstance = (AppClass) istream.readObject();
        }
        catch (java.io.IOException ioex)
        {
            // Handle deserialization exception.
        }
        finally
        {
            istream.close();
        }
    }
