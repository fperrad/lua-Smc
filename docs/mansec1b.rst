The Task FSM
------------

The Task FSM diagram is:

.. image:: ./Images/SmcTaskMap.png

The Task's states are:

  - **Running**: the task is actively doing work.
    The task is allowed to run for a specified time limit.
  - **Suspended**: the task is waiting to run again since it is not yet completed.
  - **Stopped**: the task has either completed running or externally stopped.
  - **Blocked**: the uncompleted task is externally prevented from running again.
    It will stay in this state until either stopped or unblocked.
  - **Stopping**: the task is cleaning up allocated resources before entering the stop state.
  - **Deleted**: the task is completely stopped and all associated resources returned.
    The task may now be safely deleted. This is the FSM end state.

Some notes on this FSM:

  - The ``Task`` object starts in the ``Suspended`` state.
  - The transitions match the ``TaskEventListener`` interface's methods.
  - The ``Stopped`` state is reached when either the ``Running`` task completes or is externally stopped.
  - The ``Stop``, ``Block`` and ``Delete`` transitions do not start in any specified state.
    More on that in :doc:`coding up the FSM</mansec1d>`.

Now the problem is: how to take this FSM and put it into your code?
The first step in that is encoding the FSM in the SMC language.
