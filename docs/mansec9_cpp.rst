C++
---

If you are *not* using push/pop transitions,
then the use the following code to persist the current state:

.. code-block:: c++

    int AppClass::serialize(const char *filename) const
    {
        int fd;
        int stateId(_fsm.getState().getId());
        int retcode(-1);
        
        fd = open(filename,
                  (O_WRONLY | O_CREAT | O_TRUNC),
                  (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
        if (fd >= 0)
        {
            retcode = write(fd, &stateId, sizeof(int));
            (void)close(fd);
            fd = -1;*
        }
        
        return (retcode);
    }

Deserializing is the mirror:

.. code-block:: c++

    int AppClass::deserialize(const char *filename) const
    {
        int fd;
        int stateId;
        int retcode(-1);
        
        // This code assumes _fsm is already instantiated.
        
        fd =open(filename, O_RDONLY);
        if (fd >= 0)
        {
            retcode = read(fd, &stateId, sizeof(int));
            if (retcode >= 0)
            {
                _fsm.setState(_fsm.valueOf(stateId));
            }
            (void)close(fd);
            fd = -1;
        }
        
        return (retcode);
    }

If you are using push/pop transitions,
then the serialization will be require your
to persist the state stack in reverse order (bottom to top)
followed by the current state.

.. warning::

    Reading in the state stack results in emptying the stack and corrupting the FSM.
    This should not be a problem because you are persisting the FSM
    for use later when you will restore the state stack.

.. code-block:: c++

    int AppClass::serialize(const char *filename) const
    {
        int fd;
        int retcode(-1);
        fd = open(filename,
                  (O_WRONLY | O_CREAT | O_TRUNC),
                  (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
        if (fd >= 0)
        {
            int size(_fsm.getStateStackDepth() + 1);
            int bufferSize(size *sizeof(int));
            int buffer[size + 1];
            int i;
            
            // Copy the states into the buffer in reverse order:
            // from the state stack bottom to the top and the current
            // state last. The first element is the number of states.
            buffer[0] = size;
            buffer[size] = (_fsm.getState()).getId();
            for (i = (size - 1); i > 0; --i)
            {
                _fsm.popState();
                buffer[i] = (_fsm.getState()).getId();
            }
            
            retcode = write(fd, buffer, (bufferSize + sizeof(int)));
            
            (void)close(fd);
            fd = -1;
        }
        
        return (retcode);
    }

When reading in the persisted FSM,
first read the state count and then read in the states:

.. code-block:: c++

    int AppClass::deserialize(const char *filename)
    {
        int fd;
        int size;
        int retcode(-1);
        
        // The FSM's current state is probably set to the start
        // state. Clear it out because it is not correct.
        _fsm.clearState();
        
        // Open the file for reading and then read in the number
        // of persisted states.
        fd = open(filename, O_RDONLY);
        if (fd >= 0 && read(fd, &size, sizeof(int)) >= 0)
        {
            int bufferSize(size * sizeof(int));
            int buffer[size];
            
            if (read(fd, buffer, bufferSize) >= 0)
            {
                int i;
                
                // Note: Do not call setState for the final current
                // state because pushState actually sets the current
                // state while pushing the next state on the stack.
                for (i = 0; i < size; ++i)
                {
                    _fsm.pushState(_fsm.valueOf(buffer[i]));
                }
            }
        }
        
        // Make sure the file is closed before leaving.
        if (fd >= 0)
        {
            (void)close(fd);
            fd = -1;
        }
        
        return (retcode);
    }
