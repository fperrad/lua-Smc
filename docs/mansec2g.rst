Transition Guards
-----------------

.. image:: ./Images/TransGuard.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans
        Run
        
          // Guard condition
          [ctxt.isProcessorAvailable() == true &&
           ctxt.getConnection().isOpen() == true]
        
            // Next State
            Running
            // Actions
            {
                StopTimer("Idle");
                DoWork();
            }
        
        Run     nil             {RejectRequest();}
    }

The guard must contain a condition that is valid target language source code - that is, it would be a valid ``if`` statement.
Your guard may contain &&s, ||s, comparison operators (==, <, etc.) and nested expressions.
SMC copies your guard condition verbatim into the generated output.

Note: If your are calling a context class method,
then you must prefix the method with ``ctxt`` - SMC will *not* append ``ctxt`` for you.

If the guard condition evaluates to true, then the transition is taken.
If the guard condition evaluates to false, then one of the following occurs (ordered by precedence):

  1. If the state has another guarded transition with the same name and :doc:`arguments</mansec2h>`, that transition's guard is checked.
  2. Failing that, If the state has another unguarded transition with the same name and :doc:`argument list</mansec2h>`, that transition is taken.
  3. If none of the above, then the :doc:`default transition</mansec2l>` logic is followed.

A state may have multiple transitions with the same name and :doc:`argument list</mansec2h>` as long as they all have unique guards.
When a state does have multiple transitions with the same name, care must be taken when ordering them.
The state machine compiler will check the transitions in the same top-to-bottom order that you use *except* for the unguarded version. That will always be taken only if all the guarded versions fail. Guard ordering is only important if the guards are not mutually exclusive, i.e., it is possible for multiple guards to evaluate to true for the same event.

Allowable argument types for a transition guard are the same as for a :doc:`transition action</mansec2f>`.
