Instantiating a Finite State Machine
------------------------------------

Care must be taken when instantiating an SMC-generated finite state machine.
The application class passes a its reference to the FSM context
and this reference is used when FSM actions are performed.
It is safe to instantiation an FSM within a constructor
but unsafe to enter the start state while in a constructor
because the start state entry actions will call your application object
before its constructor has completed.

.. code-block:: java

    private final AppClassContext _fsm;
    public AppClass()
    {
        // Initialize you application class.
        // Instantiate your finite state machine.
        // Note: it is safe to pass this to the the FSM
        // constructor because the FSM constructor only
        // stores this in a data member.
        _fsm = new AppClassContext(this);
    }
    
    // Enter the FSM start state after instantiating this
    // application object. 
    public void startWorking()
    {
        _fsm.enterStartState();
        return;
    }

Prior to SMC v. 6, the FSM constructor incorrectly used the *this* pointer
which meant instantiating the FSM within the application constructor could lead to incorrect behavior.
SMC v. 6 corrects this problem and it is now safe to instantiate the FSM within your application constructor.

.. warning::

    The ``enterStartState`` method should be called only once after instantiating the finite state machine
    and prior to issuing any transition.
    This method is unprotected and does not prevent its being called multiple times.
    If ``enterStartState`` is called after the first transition, then incorrect behavior may occur.
