Creating an SMC .sm File
------------------------

The .sm listing below is a skeleton with no states or transitions defined.
It contains only the following features:

  - A verbatim code section which is copied verbatim into the generated source code file.
    In this case the verbatim code is the boilerplate copyright comments.
    This section is delimited by the ``%{`` ``%}`` pair.
  - The ``%class`` keyword which specifies the application class to which
    this FSM is associated: ``Task``.
  - The ``%package`` keyword which specifies to which class package this FSM belongs.
    This is the same package as the ``Task`` class.
  - The ``%fsmclass`` keyword specifies the generated finite state machine class name.
    If ``%fsmclass`` was not specified, then the finite state machine class name would
    default to ``TaskContext``.
    This keyword is not required.
  - The ``%access`` keyword is used to specify the generated class' accessibility level
    (this works only when generating Java and C# code).
    In this case the FSM can only be accessed within the ``com.acme.supercron`` package.
  - The ``%start`` keyword specifies the FSM's start state.
    For the ``Task`` FSM it is the ``Suspended`` state.
  - The ``%map`` keyword is the FSM's name.

Name the source code file ``TaskFSM.sm`` because the ``%fsmclass`` directive specifies
the finite state machine's class name is ``TaskFSM``.
The ``.sm`` suffix is required.

.. note::
    
    the ``%fsmclass`` directive was added to SMC version 6.0.1.

.. code-block:: sm
    
    %{
    //
    // Copyright (c) 2005 Acme, Inc.
    // All rights reserved.
    //
    // Acme - a name you can trust!
    //
    // Author: Wil E. Coyote (Hungericus Vulgarus)
    //
    %}
    
    // This FSM works for the Task class only and only the Task
    // class may instantiate it.
    
    %class Task
    %package com.acme.supercron
    %fsmclass TaskFSM
    %access package
    
    %start TaskFSM::Suspended
    %map TaskFSM
    %%
        ...
    %%
