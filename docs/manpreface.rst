Preface
-------



This manual describes how to use the state machine compiler.
Hopefully, you will find this tool as useful as I have.
State machines are a natural way to describe object behavior
but there has not been an easy way to mate state machines with objects.
This compiler allows you to add a state machine to just about
any class you desire (more about that later.)

I encourage you to first look at the annotated SMC code in :doc:`section 1</mansec1>`.
This code demonstrates SMC's powerful simplicity.
The state machine code is simple, meaning you can easily learn it.
Its simplicity also allows you to readily build powerful,
robust finite state machines.

For those of you who hang around comp.lang.c++ or comp.object,
you may have read some of Bob Martin's articles.
You may have even read about his state machine compiler and gotten a copy of it.
You may then notice a striking similarity between my compiler and his.
There is a reason for that: this compiler was derived from Bob Martin's
original state machine compiler.

While an employee of Clear Communications Corporation (renamed Clear and now defunct),
he developed state machine classes and later a compiler to automatically generate those classes.
About this time, I came to work for the same company and was intrigued by what Bob had done.
Some six months later, Bob struck out on his own to form Object Mentor Associates
and I was left to maintain the state machine compiler.
I have added many features to the original system (arguments, default transitions,
push and pop transitions, transition guards, a more YACC-like language structure, etc.)

But no matter how much I have added,
the state machine compiler will always be Bob Martin's invention.
I'd like to think that I made a good thing better.
