Connecting Task and Task FSM
----------------------------


Connecting FSMs to their application class is as simple as:

  - Add the data member ``TaskFSM _fsm`` to ``Task`` class.
  - Instantiate ``TaskFSM`` in ``Task``'s constructor.
  - If the start state has entry actions that must be executed when the FSM is created,
    then call ``_fsm.enterStartState()`` outside of ``Task``'s constructor.
  - When you need to issue a transition, call ``_fsm``'s appropriate transition method:
        ``_fsm.Start(timeSlice);``

.. code-block:: java
    :emphasize-lines: 11-15,18-27,36,45,52,59,66,75,90,99-100

    package com.acme.supercron;
    
    public final class Task
        implements TaskEventListener,
                   TimerEventListener
    {
        public Task()
        {
            // Object initialization.
            ...
            // Instantiate the FSM here but perform the initial
            // state's entry actions outside of the constructor
            // to prevent referencing this object before its
            // initialization is complete.
            _fsm = new TaskFSM(this);
        }
    
        // Execute the start state's entry actions by calling this
        // method. This method should be called only once and prior to
        // issuing any transitions. Therefore this method should be
        // called before registering this Task instance as a task and
        // timer event listener.
        public void startFSM()
        {
            _fsm.enterStartState();
            TaskManager.addListener(this);
        }
    
        //-----------------------------------------------------------
        // TaskEventListener Interface Implemenation.
        //
        // Time for the incomplete task to continue its work for the
        // specified time slice.
        public void start(long timeSlice)
        {
            _fsm.Start(timeSlice);
        }
    
        // Called when a running, incomplete task should suspend
        // running even though its time slice is not expired.
        // Note: the task's running is also suspended when the time
        // slice expires.
        public void suspend()
        {
            _fsm.Suspend();
        }
    
        // Called when an incomplete task is blocked. Blocked tasks
        // are able to continue running when unblocked.
        public void block()
        {
            _fsm.Block();
        }
    
        // Called when a blocked task is unblocked and allowed
        // to continue running.
        public void unblock()
        {
            _fsm.Unblock();
        }
    
        // Called when an incomplete task is permanently stopped.
        // Stopped tasks are then deleted.
        public void stop()
        {
            _fsm.Stop();
        }
    
        // Called when the task is deleted. Tasks are deleted when
        // either 1) the task has completed running and is now
        // stopped or 2) when the system is shutting down and all
        // are to terminate immediately.
        public void delete()
        {
            _fsm.Delete();
        }
    
        //
        // end of TaskEventListener Interface Implemenation.
        //-----------------------------------------------------------
    
        //-----------------------------------------------------------
        // TimerEventListener Interface Implementation.
        //
    
        // Called with the time slice timer has expired. If running,
        // the task is suspended.
        public void handleTimeout(TimerEvent event)
        {
            _fsm.Suspend();
        }
    
        //
        // end of TimerEventListener Interface Implementation.
        //-----------------------------------------------------------
    
    <snip>
    
        // The associated finite state machine.
        private final TaskFSM _fsm;
    }

Voila! ``Task``'s behavior is now defined by a finite state machine.
