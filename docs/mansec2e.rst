An Internal Loopback Transition
-------------------------------

.. image:: ./Images/InternalLoopback.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans    Next State      Actions
        Timeout     nil             {}
    }

Using ``nil`` as the next state causes the transition to remain in the current state and not leave it.
This means that the state's :doc:`exit and entry</mansec2i>` actions are *not executed*.
This is in contrast to an :doc:`external loopback</mansec2d>` transition.
