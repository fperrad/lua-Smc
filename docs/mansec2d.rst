An External Loopback Transition
-------------------------------

.. image:: ./Images/Loopback.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans    Next State      Actions
        Timeout     Idle            {}
    }

An external loopback does leave the current state and comes back to it.
This means that the state's :doc:`exit and entry</mansec2i>` actions are executed.
This is in contrast to an :doc:`internal loopback</mansec2e>` transition.
