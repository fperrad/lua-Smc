Queuing Up
----------

SMC does not allow a transition to issue a transition itself.
The reason for this is explained in SMC's FAQ.
Put if it absolutely necessary for you to do so,
this section explains how you can do so
by creating your own transition queue in C++, Java, Tcl, VB.net and C#.

.. warning::

    The following code assumes that you are *not* using transition arguments in your code.
    While it is possible to do transition queuing together with transition arguments,
    it is more difficult and error prone.

.. toctree::

    mansec7_cpp.rst
    mansec7_java.rst
    mansec7_tcl.rst
    mansec7_vb.rst
    mansec7_cs.rst
