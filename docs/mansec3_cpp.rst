C++
---

**Assumptions:**

  - Your application class is named ``NetworkIF``.
  - Your class is stored in ``NetworkIF.h`` and ``NetworkIF.cpp``.
  - The state machine is in ``NetworkIF.sm``.
  - As explained in :doc:`section 1 </mansec1>`, you have added the lines
    
    .. code-block:: sm
        
        %class NetworkIF
        %header NetworkIF.h
        %start MainMap::Start
    
    at the :doc:`appropriate</mansec1c>` location in ``NetworkIF.sm``.
  - You have successfully :doc:`compiled</mansec4>` ``NetworkIF.sm``.

**Changes to NetworkIF.h**

  1. Add the following #include to NetworkIF.h:
     
     .. code-block:: c++
         
         #include "NetworkIF_sm.h"
     
  2. Add the member data to class NetworkIF:
     
     .. code-block:: c++
         
         NetworkIFContext _fsm;
     
     This member data can be either public,
     protected or private and can have any name
     (I used ``_fsm`` but you can use another name).

  3. All state machine actions must be implemented as ``NetworkIF`` **public** methods.
     For an explanation as to why these methods must be public,
     see the answer to the FAQ question: Why do actions have to be declared as public?

**Changes to NetworkIF.cpp**

  1. In all NetworkIF constructors,
     add the following to the initialization list:
     
     .. code-block:: c++
         
         _fsm(*this)
     
     Again, the member data does not have to be named ``_fsm``.
  2. If the start state has entry actions which must be executed
     before any transitions are issued add the following code:
     
     .. code-block:: c++
         
         _fsm.enterStartState();
     
  3. Wherever you want to issue a state machine transition,
     add the following line of code:
     
     .. code-block:: c++
         
         _fsm.<transition>();
     
     Where "<transition>" is the name of the transition you want to take.
     If the transition takes arguments then pass them in the transition call:
     
     .. code-block:: c++
         
         _fsm.Connect("192.168.3.100", 80);
     
**Changes to Makefile**

Add ``NetworkIF_sm.cpp`` to the source file list
and link ``NetworkIF_sm.o`` into your application.

**"using namespace" in NetworkIF.sm**

If you need to place the SMC-generated classes into a C++ namespace,
then see :doc:`section 8</mansec8>`.

**#includes in NetworkIF.sm**

If you need to include header files in your .sm file, use the ``%include`` keyword:

.. code-block:: sm
    
    %include <util/logger.h>

**Cautions**

The following class and file names are generated by SMC.
Be careful not to use them yourself.

  - <AppClass>Context
  - <AppClass>State
  - <MapName>
  - <MapName>_Default
  - <MapName>_<StateName>
  - <smc file name stem>_sm.h
  - <smc file name stem>_sm.<ext>

where:

  - <AppClass> is the name of your application class using that FSM.
  - <MapName> is a ``%map <MapName>`` in <smc file name stem>.sm.
  - <StateName> is a state in <smc file name stem>.sm.
  - <smc file name stem> is that part of the .sm file's name before the ".".
  - <ext> is the source file extension (defaults to "cpp").
