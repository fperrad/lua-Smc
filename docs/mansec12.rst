Getting Noticed
---------------

SMC uses the Java Bean event notification and .Net event raising features
to inform listeners when an SMC-generated finite state machine changes state.
The following sample code demonstrates how to use this feature in Java,
C#, VB.net, Groovy and Scala.

.. toctree::

    mansec12_java.rst
    mansec12_cs.rst
    mansec12_vb.rst

