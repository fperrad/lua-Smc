Push Transition
---------------

.. image:: ./Images/PushTrans.png

.. code-block:: sm

    Running
    {
        Blocked     push(WaitMap::Blocked)              {GetResource();}
    }

.. note::

    The end state does not have to be in another map - it could be in the same ``%map`` construct.
    Conversely, a plain transition's end state may be in another map.
    But chances are that you will set up maps so that you will push
    to another map's state and simple transitions will stay within the same map.
    You use multiple maps for the same reason you create multiple subroutines:
    to separate out functionality into easy-to-understand pieces.

With SMC v. 1.3.2, the push syntax was modified yet is backward compatible with the initial syntax.
The new syntax is

.. code-block:: sm

    Running
    {
        Blocked     BlockPop/push(WaitMap::Blocked)     {GetResource();}
    }

This causes the state machine to:

  1. Transition to the ``BlockPop`` state.
  2. Execute the ``BlockPop`` entry actions.
  3. Push to the ``WaitMap::Blocked`` state.
  4. Execute the ``WaitMap::Blocked`` entry actions.

When ``WaitMap`` issues a :doc:`pop</mansec2k>` transition,
control will return to ``BlockPop`` and the pop transition is issued from there.

Use this new syntax when a state has two different transitions
which push to the same state but need to handle the pop transition differently.
For example:

.. code-block:: sm

    Idle
    {
        NewTask     NewTask/push(DoTask)    {}
        RestartTask OldTask/push(DoTask)    {}
    }
    NewTask
    {
        TaskDone    Idle                    {}
    
        // Try running the task one more time. 
        TaskFailed  OldTask/push(DoTask)    {}
    }
    OldTask
    {
        TaskDone    Idle                    {}
        TaskFailed  Idle                    {logFailure();}
    }
