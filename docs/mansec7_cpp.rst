C++
---

The following code should be added to a class which has an associated FSM
and which will be issuing transitions from a transition action.

(The example code is based on examples/C++/EX1.)

  1. **Add a transition enum private member.**
     The enum will contain one entry for each unique transition.
     
     .. code-block:: c++
     
         private:
         
             enum Transitions {
                 ZERO_TRANS = 1,
                 ONE_TRANS,
                 UNKNOWN_TRANS,
                 EOS_TRANS
             };
     
  2. **Add a transition queue private member.**
     Enqueue enum values here for later execution rather the calling the transition methods directly.
     
     .. code-block:: c++
     
         private:
         
         Queue<list<int> > _transitionQueue;
     
  3. **Add a private transition method.**
     This method first places a transition on the queue and,
     only if it detects this call is outside a transition,
     dequeues a transition and executes it.
     
     .. code-block:: c++
     
         private:
         
             void transition(int transition)
             {
                 _transitionQueue.push_back(transition);
                 
                 if (_fsm.isInTransition() == false)
                 {
                     while (_transitionQueue.empty() == false)
                     {
                         transition = _transitionQueue.pop_front();
                         
                         switch (transition)
                         {
                             case ZERO_TRANS:
                                 _fsm.Zero();
                                 break;
                             
                             case ONE_TRANS:
                                 _fsm.One();
                                 break;
                             
                             case UNKNOWN_TRANS:
                                 _fsm.Unknown();
                                 break;
                             
                             case EOS_TRANS:
                                 _fsm.EOS();
                                 break;
                         }
                     }
                 }
                 
                 return;
             }
     
  4. **Replace transition calls.**
     Replace your transition method calls:
     
     ``_fsm.Zero();``
     
     with transition method calls:
     
     ``transition(ZERO_TRANS);``
