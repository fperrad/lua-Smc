.. SMC documentation master file, created by
   sphinx-quickstart on Sun Jan  5 11:09:01 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SMC Programmer's Manual
==================================

:Release: |version|
:Date: |today|

Contents:

.. toctree::
    :maxdepth: 2

    manpreface.rst
    mansec1.rst
    mansec2.rst
    mansec3.rst
    mansec4.rst
    mansec5.rst
    mansec6.rst
    mansec7.rst
    mansec8.rst
    mansec9.rst
    mansec10.rst
    mansec11.rst
    mansec12.rst
    mansec13.rst
    manappa.rst
    demo.rst
