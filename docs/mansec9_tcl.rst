Tcl
---

.. note::

    the following sample code requires the .sm file is compiled with the **-serial** option.

Tcl persistance is similar to C++.
If you are *not* using push/pop transitions,
then the use the following code to persist the current state:

.. code-block:: tcl

    public method serialize {fileName} {
        if [catch {open $fileName w 0644} fileId] {
            set retcode error;
            set retval "Failed to open ${filename} for writing";
        } else {
            puts $fileId [[$_fsm getState] getId];
            close $fileId;
            
            set retcode ok;
            set retval "";
        }
        
        return -code ${retcode} ${retval};
    }

The following deserializes the current state:

.. code-block:: tcl

    public method deserialize {fileName} {
        if [catch {open $fileName r} fileId] {
            set retcode error;
            set retval "Failed to open ${filename} for reading";
        } else {
            gets $fileId stateId;
            $_fsm setState [$_fsm valueOf $stateId];
            
            close $fileId;
            
            set retcode ok;
            set retval "";
        }
        
        return -code ${retcode} ${retval};
    }

If you are using push/pop transitions,
then the serialization will be require your to persist
the state stack in reverse order (bottom to top) followed by the current state.

.. warning::

    Reading in the state stack results in emptying the stack and corrupting the FSM.
    This should not be a problem because you are persisting the FSM for use later
    when you will restore the state stack.

.. code-block:: tcl

    public method serialize {fileName} {
        if [catch {open $fileName w 0644} fileId] {
            set retcode error;
            set retval "${fileName} open failed";
        } else {
            set state [$_fsm getState];
            set states {};
            
            lappend states [$state getId];
            while {[catch {$_fsm popState} retcode] == 0} {
                set state [$_fsm getState];
                set states [linsert$states 0 [$state getId]];
            }
            
            set size [llength $states];
            puts $fileId $size;
            
            foreach stateId $states {
                puts $fileId $stateId;
            }
            
            close $fileId;
            
            set retcode ok;
            set retval "";
        }
        
        return -code ${retcode} ${retval};
    }

When reading in the persisted FSM,
first read the state count and then read in the states:

.. code-block:: tcl

    public method deserialize {fileName} {
        if [catch {open $fileName r} fileId] {
            set retcode error;
            set retval "${fileName} open failed";
        } else {
            # Clear out the default start state.
            $_fsm clearState;
            
            gets $fileId size;
            for {set i 0} {$i < $size} {incr i} {
                gets $fileId stateId;
                set state [$_fsm valueOf $stateId];
                
                $_fsm pushState $state;
            }
            
            close $fileId;
            set retcode ok;
            
            set retval "";
        }
        
        return -code ${retcode} ${retval};
    }
