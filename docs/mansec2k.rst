Pop Transition
--------------

.. image:: ./Images/PopTrans.png

.. code-block:: sm

    Waiting
    {
        Granted     pop(OK)         {cleanUp();}
        Denied      pop(FAILED)     {cleanUp();}
    }

The pop transition differs from the simple and push transition in that:

  - The end state is not specified.
    That is because the pop transition will return to whatever state issued the corresponding push.
  - There pop transition has an *optional* argument: a transition name.

In the above example, if the resource request is granted, the state machine returns
to the corresponding state that did the push and then takes that state's ``OK`` transition.
If the request is denied, the same thing happens except the ``FAILED`` transition is taken.
The code for the corresponding push transition is:

.. code-block:: sm

    Running
    {
        Blocked     push(WaitMap::Blocked)  {GetResource();}
    
        // Handle the return "transitions" from WaitMap.
        OK          nil                     {}
        FAILED      Idle                    {Abend(INSUFFICIENT_RESOURCES);}
    }

As of SMC v. 1.2.0, additional arguments may be added *after* the pop transition's transition argument.
These additional arguments are like any others passed to an action and will be passed into the named transition.
Following the above example, given the pop transition ``pop(FAILED, errorCode, reason)``,
then the ``FAILED`` should be coded as:

.. code-block:: sm

    FAILED(errorCode: ErrorCode, reason: string)
        Idle
        {
            Abend(errorCode, reason);
        }

