Compiling a .sm
---------------

These instructions assume that:

  - Java 1.6.0 or newer is properly installed and the "javac", "java" and "jar" executables
    are in your PATH environment variable.
    The standard Java Development Kit can be obtained for free from http://java.sun.com .
  - The ``SMC_HOME`` environment variable contains the path to where SMC is installed.

The steps to compiling a .sm file are:

  1. From a shell (Unix or Windows CMD), go to the directory containing the .sm file.
  2. ``$ java -jar $SMC_HOME/bin/Smc.jar [-c | -c++ | -csharp | -graph | -groovy | -java | -lua | -objc | -perl | -php | -python | -ruby | -scala | -table | -tcl | -vb] <fsm_source_file>.sm``

You must specify one of:

  - -c
  - -c++
  - -csharp
  - -graph
  - -groovy
  - -java
  - -lua
  - -objc
  - -perl
  - -php
  - -python
  - -ruby
  - -scala
  - -table
  - -tcl
  - -vb

so SMC will know what source code to generate.
Failure to do specify exactly one will result in an error and the state machine file will not be compiled.

smc's command line options are:

  - -help: Prints out SMC's command line usage to the console and exits without doing any compiling.
  - -version: Print out SMC's version information to the console and exits without doing any compiling.
  - -verbose: SMC outputs more messages as it compiles.
  - -c: Generate C code.
  - -c++: Generate C++ code.
  - -csharp: Generate C# code.
  - -graph: Generating a Graphviz DOT file representing the finite state machine.
  - -groovy: Generate Groovy code.
  - -java: Generate Java code.
  - -lua: Generate Lua code.
  - -objc: Generates Objective-C code.
  - -perl: Generate Perl code.
  - -php: Generate PHP code.
  - -python: Generate Python code.
  - -ruby: Generate Ruby code.
  - -scala: Generate Scala code.
  - -table: Generate an HTML table representing the finite state machine.
  - -tcl: Generate [incr Tcl] code.
  - -vb: Generate VB.net code.
  - -suffix: Specify what suffix to be used on the output source file. Default suffixes are:
       - C: c and .h
       - C++: cpp and .h
       - C#: cs
       - graph: dot
       - Groovy: groovy
       - Java: java
       - Lua: lua
       - Objective-C: .m and .h
       - Perl: pm
       - PHP: php
       - Python: py
       - Ruby: rb
       - Scala: scala
       - table: html
       - Tcl: tcl
       - VB.net: vb
  - -sync: (May be used only with the -java, -groovy, -scala, -vb and -csharp options.) Causes SMC to:
       - Java: add the synchronized keyword to the transition method declarations.
       - Groovy: add the synchronized keyword to the transition method declarations.
       - Scala: add the synchronized keyword to the transition method declarations.
       - VB.net: encapsulate the transition method's body in a SyncLock Me, End SyncLock block.
       - C#: encapsulate the transition method's body in a lock(this) {...} block.
  - -noex: (May be used only with the -c++ command line option.)
    Causes SMC to generate assert()s rather than exception throws when unrecoverable errors are detected
    (e.g. when an action issues a transition from within a transition).
  - -nocatch: Do not generate try/catch/rethrow code.
    This is not recommended because SMC uses this code to make sure the current state is set
    before passing the exception to the application. Use this in C++ applications where exceptions are not used.
  - -cast: (May be used only with the -c++ command line option.)
    Specifies what casting operator is used in the generate C++ code.
    The default is dynamic_cast.
    If runtime type identification (RTTI) is not used then use this option to specify another.
  - -d: Specifies where SMC should place generated files.
    If not specified, then generated files are placed in the same directory as the .sm file.
  - -headerd: (May be used only with the -c, -c++ and -objc command line options.)
    Specifies where SMC should place .h generated files.
    If not specified but -d is specified, then .h files is placed in the -d directory.
    If neither -headerd nor -d is specified, then .h files are placed in the same directory as the .sm file.
  - -nostreams: (May be used only with the -c++ and -g command line options.)
    The generated C++ code will not use IOStreams for debug output.
    Instead the application must provide the following subroutine/macro:
    
    ``void TRACE(const string& fmt, ...)``
    The signature is just like printf(3).
    The subroutine/macro is responsible for outputting fmt.
  - -serial: Generate unique integer IDs for each state.
    These IDs can be used when persisting an FSM.
  - -g, -g0, -g1: Adds debug output messages to the generated code.
    -g0 produces output messages which signal when the FSM has exited a state, entered a state, entered and exited a transition.
    -g1 includes the -g0 output and addition output for entering and exiting state Exit and Entry actions.
    -g is the same as -g0.

    .. note::

        These message are automatically printed out.
        To see them you must turn them on with the following code in your application class:
        
          - C: setDebugFlag(&this->_fsm, 1);
          - C++: _fsm->setDebugFlag(true);
          - C#: _fsm.DebugFlag = true
          - Graph: Ignores this option.
          - Java: _fsm.setDebugFlag(true);
          - Lua: self._fsm:setDebugFlag(true)
          - Objective-C: [_fsm setDebugFlag:YES]
          - Perl: $self->{_fsm}->setDebugFlag(1);
          - PHP: $this->_fsm->setDebugFlag(true);
          - Groovy: _fsm.setDebugFlag(true) or _fsm.debugFlag = true
          - Python: self._fsm.setDebugFlag(true)
          - Ruby: @_fsm.setDebugFlag(true)
          - Scala: _fsm.setDebugFlag(true)
          - Table: Ignores this option.
          - Tcl: $_fsm setDebugFlag 1
          - VB.net: _fsm.DebugFlag = True

  - -reflection: (May be used only with -csharp, -groovy, -java, -lua, -perl, -php, -python, -ruby, -scala, -tcl and -vb.) Causes SMC to generate a getTransitions method, allowing applications to query the available transitions for the current state. Go here for more information on SMC reflection..
  - -generic: (May be used only with either -csharp, -java or -vb and -reflection.) Causes SMC to use generic collections for reflection.
  - -generic7: (May be used only with either -java and -reflection.) Causes SMC to use Java 7 generic collections and <> braces for reflection.
    May not use -generic and -generic7 together.
  - -glevel: (May be used only with the -graph command line option.) Specifies how much detail to place into the DOT file. Level 0 is the least detail and 2 is the greatest. See section 10: Get the Picture? for detailed explanation of what each graph level provides.

