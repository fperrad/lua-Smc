A Transition with Actions
-------------------------

.. image:: ./Images/TransAct.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans
        Run
            // Next State 
            Running
            // Actions
            {
                StopTimer("Idle");
                DoWork();
            }
    }

Rules for actions:

  1. A transition's actions must be enclosed in a ``{ }`` pair.
  2. The action's form is ``[A-Za-z][A-Za-z0-9_-]*(<argument list>)``.
     The argument list must be either empty or consist of comma-separated literals.
     Examples of literals are: integers (positive or negative, decimal, octal or hexadecimal),
     floating point, strings enclosed in double quotes, constants and :doc:`transition arguments</mansec2h>`.
  3. Actions must be member functions in the ``%class`` class and accessible by the state machine.
     Usually that means public member functions in C++ or package in Java.

Action arguments include:

  - An integer number (e.g. 1234).
  - A float number (e.g. 12.34).
  - A string (e.g. "abcde").
  - A :doc:`transition argument</mansec2h>`.
  - A constant, #define or global variable.
  - An independent subroutine or method call (e.g. event.getType()).
        .. note::

            this subroutine/method call may also include arguments.

If you want to call a method in the :doc:`context class</mansec1a>`,
then use the ``ctxt`` variable.
For example, if the context class contains a method ``getName()``
and you want to call it inside an action's argument list,
then write ``ctxt.getName()``.

Go :doc:`here</mansec1a>` for sample code using the ``ctxt`` variable.

.. note::

    Only use ``ctxt`` inside argument lists and :doc:`transition guards</mansec2g>`.
