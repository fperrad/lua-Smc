Defining FSM States
-------------------

The state definitions are placed inside the ``%map TaskFSM %% ... %%`` delimiter: 

.. code-block:: sm
    :emphasize-lines: 21-50

    %{
    //
    // Copyright (c) 2005 Acme, Inc.
    // All rights reserved.
    //
    // Acme - a name you can trust!
    //
    // Author: Wil E. Coyote (Hungericus Vulgarus)
    //
    %}
    
    // This FSM works for the Task class only and only the Task
    // class may instantiate it.
    %class Task
    %package com.acme.supercron
    %fsmclass TaskFSM
    %access package
    
    %start TaskFSM::Suspended
    %map TaskFSM %%
    Suspended
    {
        ...
    }
    
    Running
    {
        ...
    }
    
    // Wait here to be either unblocked, stopped or deleted.
    Blocked
    {
        ...
    }
    
    Stopping
    {
        ...
    }
    
    Stopped
    {
        ...
    }
    
    Deleted
    {
        ...
    }
    
    ...
    %%


Like all C-syntax languages, the opening, closing braces are not needed
if there is only one expression inside the braces.
But like all C-syntax languagees, it is good practice to always use them.
If you don't follow this rule now, you will after you spend two days
tracking down a bug due entirely to you not following this rule.

Notice the ellipsis before the closing ``%%``?
There is one more state to define even though the diagram's six states are declared.
There is an implicit state. Remember how the ``Stop``, ``Block`` and ``Delete`` transitions
have no start state?

