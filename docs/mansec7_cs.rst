C#
--

The following code should be added to a class which has an associated FSM
and which will be issuing transitions from a transition action.

(The examples code is based on examples/CSharp/EX1.)

  1. **Add a transition table private member.**
     This table is a list of all transition methods.
     The table is static because the FSM is common for all instances of the context class.
     
     .. code-block:: c#
         
         using System.Collections;
         ...
         private static Hashtable _transition_map;
     
  2. **Add a transition queue private member.**
     Place transitions here for later execution.
     One transition queue is needed for each context class instance.
     
     .. code-block:: c#
         
         private Queue _transition_queue;
     
  3. **Find all transition methods.**
     In the class static block, fill in the transition table
     with the transition methods using This method first places a transition
     on the queue and executes it only if it detects there is no transition
     in progress is the specified transition dequeued and executed.
     
     .. code-block:: c#
         
         using System.Reflection;
         ...
         static AppClass()
         {
             try
             {
                 Type context = Type.GetType("AppClassContext");
                 MethodInfo[] transitions = context.GetMethods();
                 string name;
                 int i;
                 
                 for (i = 0; i < transitions.Length; i++)
                 {
                     // Ignore the getState and getOwner methods.
                     if (transitions[i].Name != "getState" &&
                         transitions[i].Name != "getOwner")
                     {
                         _transition_map.Add( transitions[i].Name, transitions[i]);
                     }
                 }
             }
             catch
             {}
         }
      
  4. **Add a transition method.**
     Queues up a transition and issues it only if the FSM is not currently in a transition:
     
     .. code-block:: c#
         
         internal void transition(string trans_name)
         {
             lock (_transition_queue)
             {
                 string name;
                 Method transition;
                 
                 // Add the transition to the queue.
                 _transition_queue.Enqueue(trans_name);
                 
                 // Only if a transition is not in progress should a
                 // transition be issued.
                 while (_fsm.isInTransition() == false &&
                        _transition_queue.Count > 0)
                 {
                     name = (string) _transition_queue.Dequeue();
                     transition = (MethodInfo) _transition_map[name];
                     try
                     {
                         transition.Invoke(_fsm, null);
                     }
                     catch
                     {
                         // Handle exception.
                     }
                 }
             }
             
             return;
         }
     
  5. **Replace transition calls.** Replace your transition method calls:
     
     ``_fsm.Zero()``
     
     with transition method calls:
     
     ``transition("Zero")``
