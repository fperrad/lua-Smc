On Reflection
-------------

SMC v. 4.3.0 introduces the **-reflect** command line option for the Java,
C#, Perl, PHP, Python, Ruby, Tcl and VB.Net programming languages.
This option tells SMC to generate for each state class either
a ``getTransitions`` method or a ``Transitions`` property for C# and VB.Net.
In both cases the method/property returns the state's defined transitions as a map.
The map key is a transition name with an integer value.

SMC v. 6.0.1 adds the ability to retrieve a list of all states in the finite state machine.

The method definitions are:

  - **C#**: The top-level context class contains the get-only property:
    
    ``public <context>State[] States``
    
    For each of the states, there is the get-only property:
    
    ``public System.Collections.IDictionary Transitions``
    
    where the key is a string and the value is an int.
    
    .. note::
    
        if the **-generic** option is used, then SMC emits:
        
        ``public System.Collections.IDictionary<string, int> Transitions``
    
  - **Groovy**: The top-level context class contains:
    
    ``final states = [ ... ]``
    
    a array of state.
    
    ``final transitions = [ ... ]``
    
    a array of string.
    
    For each of the states, the transitions map is defined:
    
    ``final transitions = [<transition>:<integer value>]``
    
  - **Java**: The top-level context class contains the method:
    
    ``public <context>State[] getStates()``
    
    For each of the states, there is the method:
    
    ``public Map getTransitions()``
    
    where the key is a String and the value is an Integer.
    
    .. note::
    
        if the **-generic** option is used, then SMC emits:
        
        ``public Map<String, Integer> getTransitions()``
        
        When the **-generic7** option is used,
        the SMC emits the ``<>`` braces when instantiating generic collection objects.
    
    The top-level context class has a method to return
    the set of all transitions within the finite state machine:
    
    ``public Set getTransitions``
    
    or:
    
    ``public Set<String> getTransitions``
    
  - **Lua**: The top-level context class contains the methods:
    
    ``function getStates()``
    
    which returns a array.
    
    ``function getTransitions()``
    
    which returns a array of string.
    
    For each of the states, there is the method:
    
    ``function getTransitions()``
    
    which returns a table where the key is a string and the value is an integer.
    
  - **Perl**: The top-level context class contains the methods:
    
    ``sub getStates``
    
    which returns a array.
    
    ``sub getTransitions``
    
    which returns a array of string.
    
    For each of the states, there is the method:
    
    ``sub getTransitions()``
    
    which returns a hash where the key is a string and the value is an integer.
    
  - **PHP**: The top-level context class contains the methods:
    
    ``public function getStates()``
    
    which returns a array.
    
    ``public function getTransitions()``
    
    which returns a array of string.
    
    For each of the states, there is the method:

    ``public function getTransitions()``
    
    which returns an associative array where the key is a string and the value is an integer.
    
  - **Python**: The top-level context class contains the methods:
    
    ``def getStates(self)``
    
    which returns a array.
    
    ``def getTransitions(self)``
    
    which returns a array of string.
    
    For each of the states, there is the method:
    
    ``def getTransitions(self)``
    
    which returns a dict where the key is a string and the value is an integer.
    
  - **Ruby**: The top-level context class contains the methods:
    
    ``def getStates()``
    
    which returns a array.
    
    ``def getTransitions()``
    
    which returns a array of string.
    
    For each of the states, there is the method:
    
    ``def getTransitions()``
    
    which returns a hash where the key is a string and the value is an integer.
    
  - **Scala**: The top-level context class contains the methods:
    
    ``def getStates(): List[<context>State]``
    
    which returns a List of state.
    
    ``def getTransitions(): List[String]``
    
    which returns a List of String.
    
    For each of the states, there is the method:
    
    ``getTransitions(): Map[String, Int]``
    
    which returns a map where the key is a String and the value is an Int.
    
  - **[incr Tcl]**: The top-level context class contains the method:
    
    ``public method getStates{}``
    
    returns a list of state instances.
    
    For each state, the following method is defined:
    
    ``public method getTransitions {}``
    
    returns a Tcl array of transition name, integer value pairs.
    
  - **VB.Net**: The top-level context class contains the method:
    
    ``Public Property States() As <context>State()``
    
    Each of the states contains the method:
    
    ``Public ReadOnly Property Transitions() As IDictionary``
    
    where the key is a String and the value is an integer.
    
    .. note::
        if the **-generic** option is used, then SMC emits:
        
        ``Public ReadOnly Property Transitions() As IDictionary(Of String, Integer)``

The transition's associated integer value is limited to:

  - The transition is undefined in the current state.
  - The transition is defined in the current state.
  - The transitions is defined in the default state.

This allows an application to discover which transitions a state supports.

.. note::

    the returned transition names map includes the ``Default`` transition.

This feature is most useful for user interface developers
who need to activate and deactivate features based on the current state.
The user's actions are limited to the current state's transitions.

.. note::

    Reminder: You cannot call ``getState()`` while in a transition
    because the state is not set.
    If need to determine which state you just left while in transition.
    call ``getPreviousState()``.

.. rubric:: Java Sample

.. code-block:: java
    
    import java.util.Iterator;
    import java.util.Map;
    
    public void processEvent(EventObject event)
    {
        // Transform this event into a FSM transition:
        if (event instanceof OpenEvent)
        {
            _fsm.openFile((OpenEvent) event);
        }
        else if ...
        {
        }
        
        // Now figure out what features are active
        // and inactive based on the current state's returned transition map.
        Map<String, Integer> transitions = (_fsm.getState()).getTransitions();
        String transition;
        int status;
        
        for (Map.Entry<String, Integer> entry: transitions.entrySet())
        {
            transition = entry.getKey();
            status = (entry.getValue()).intValue();
            
            // Ignore the Default transition.
            if (transitions.equals("Default") == true)
            {
                // no-op
            }
            // 0 - transition is undefined.
            // 1 - transition is explicity defined in the state.
            // 2 - transition is defined in Default state.
            else if (status == 0)
            {
                // Deactivate feature.
            }
            else if (status == 1)
            {
                // Activate feature.
            }
            else
            {
                // Do something else for default definitions.
            }
        }
    }

.. rubric:: C# Sample

.. code-block:: c#
    
    using System.Collections.Generic;
    
    public void processEvent(Object event)
    {
        // Transform this event into a FSM transition:
        if (event is OpenEvent)
        {
            _fsm.openFile(event);
        }
        else if ...
        
        // Now figure out what features are active
        // and inactive based on the returned transition map.
        IDictionary<string, int> transitions = (_fsm.getState()).Transitions;
        int status;
        
        foreach (string name in transitions.Keys)
        {
            status = transitions[name];
            
            // Ignore the Default transition.
            if (name == "Default")
            {
                // no-op
            }
            // 0 - transition is undefined.
            // 1 - transition is explicity defined in the state.
            // 2 - transition is defined in Default state.
            else if (status == 0)
            {
                // Deactivate feature.
            }
            else if (status == 1)
            {
                // Activate feature.
            }
            else
            {
                // Do something else for default definitions.
            }
        }
    }

.. rubric:: Tcl Sample

.. code-block:: tcl

    Using System.Collections
    
    public method ProcessEvent{event} {
        # Transform this event into a FSM transition:
        if {$event = "open"} {
            _fsm.Open()
        } elseif {...} {
            ...
        }
        
        # Now figure out what features are active
        # and inactive based on the returned transition map.
        foreach {name status} [[_fsm getState] transitions] {
            # Ignore the Default transition.
            # 0 - transition is undefined.
            # 1 - transition is explicity defined in the state.
            # 2 - transition is defined in Default state.
            if {$name = "Default"} {
                # no-op
            } elseif {$status = 0} {
                # Deactivate feature.
            } elseif {$status = 1} {
                # Activate feature.
            } else {
                # Do something else for default definitions.
            }
        }
    }
