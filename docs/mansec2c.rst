A Jump Transition
-----------------

.. image:: ./Images/JumpTrans.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans    Next State      Actions
        Run         jump(Running)   {}
    }

The Jump transition is equivalent to the :doc:`Simple Transition</mansec2b>`
and is provided since this transition is used in
an `Augmented Transition Network <http://en.wikipedia.org/wiki/Augmented_transition_network>`_.

.. warning::

    In a future SMC release, the Jump transition will become
    the only way for make an end state outside the current map.
    With the syntax : ``jump(AnotherMap::NextState)``
