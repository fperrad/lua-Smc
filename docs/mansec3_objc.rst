Objective-C
-----------

**Assumptions:**

  - Your application class is named ``NetworkIF``.
  - Your class is declared in ``NetworkIF.h`` and defined in ``NetworkIF.m``.
  - The state machine is in ``NetworkIF.sm``.
  - As explained in :doc:`section 1 </mansec1>`, you have added the lines
    
    .. code-block:: sm
        
        %class NetworkIF
        %include NetworkIF.h
        %start MainMap::Start

    at the :doc:`appropriate</mansec1c>` location in ``NetworkIF.sm``.
  - You have successfully :doc:`compiled</mansec4>` ``NetworkIF.sm``.

**Changes to NetworkIF.h**

  1. Forward declare the FSM class by adding this line to ``NetworkIF.h``:
     
     .. code-block:: objc
     
         @class NetworkIFContext;
  2. Add the member data to the class ``NetworkIF``:
     
     .. code-block:: objc
     
         NetworkIFContext *_fsm;
     
**Changes to NetworkIF.m**

  1. Import the FSM header:
     
     .. code-block:: objc
     
         #import "NetworkIF_sm.h"
     
  2.  When allocating the FSM, pass in the NetworkIF instance reference:
     
     .. code-block:: objc
     
         _fsm = [[NetworkIF alloc] initWithOwner:self];
     
  3. If the start state has entry actions which must be executed prior
     to issuing transitions add the following code:
     
     .. code-block:: objc
     
         TODO
         PUT Objective-C code here.
     
  4. When you want to issue a state machine transition,
     add the following line of code:
     
     .. code-block:: objc
     
         [_fsm <transition>];
     
     Where "<transition>" is the transition name.
