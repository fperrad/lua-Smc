VB.net
------

The following code should be added to a class which has an associated FSM
and which will be issuing transitions from a transition action.

(The examples code is based on examples/VB/EX1.)

  1. **Add a transition enum private member.**
     This table is a list of all transition methods.
     The table is Shared because the FSM is common for all instances of the context class.
     
     .. code-block:: vbnet
         
         Private Enum Transition As Integer = _
         { _
             ZERO_TRANS, _
             ONE_TRANS, _
             UNKNOWN_TRANS, _
             EOS_TRANS _
         }
     
  2. **Add a transition queue private member.**
     Place transitions here for later execution.
     One transition queue is needed for each context class instance.
     
     .. code-block:: vbnet
         
         Imports System.Collections
         ...
         Private _transitionQueue As ArrayList
     
  3. **Add a private transition method.**
     This method first places a transition on the queue
     and executes it only if it detects there is no transition
     in progress is the specified transition dequeued and executed.
     
     .. code-block:: vbnet
         
         Private Sub Transition(ByVal transition As Integer)
             _transitionQueue.Add(transition)
             While _fsm.IsInTransition = False And _transitionQueue.Count = 0
                 transition = _transitionQueue.RemoveAt(0)
                 Select Case transition
                     Case Transition.ZERO_TRANS
                         _fsm.Zero()
                     Case Transition.ONE_TRANS
                         _fsm.One()
                     Case Transition.UNKNOWN_TRANS
                         _fsm.Unknown()
                     Case Transition.EOS_TRANS
                         _fsm.EOS()
                 End Select
             End While
         End Sub
     
  4. **Replace transition calls.**
     Replace your transition method calls:
     
     ``_fsm.Zero()``
     
     with transition method calls:
     
     ``transition(Transition.ZERO_TRANS)``
