VB.Net Sample
-------------

State change event handlers register indirectly
via these application class methods (which you must add to your code).

.. code-block:: vbnet
    
    Public Class AppClass
        ...
        
        Public Sub AddStateChangeHandler(handler As statemap.StateChangeEventHandler)
            
            AddHandler _fsm.StateChange, handler
        End Sub
        
        Public Sub RemoveStateChangeHandler(handler As statemap.StateChangeEventHandler)
            
            RemoveHandler _fsm.StateChange, handler
        End Sub
    
    End Class

If a class wishes to receive state change events,
then it must implement a method with the following signature.

See the :doc:`C# sample</mansec12_cs>` for more about ``StateChangeEventArgs``.

.. code-block:: vbnet
    
    Imports System
    Imports statemap Public Class StateChangeHandler
        ...
        
        Public Sub StateChange(sender As Object, e As StateChangeEventArgs)
            
            Console.Write("FSM ")
            Console.Write(e.FSMName())
            Console.Write(" ")
            Console.Write(e.TransitionType())
            Console.Write(" transition from ")
            Console.Write(e.PreviousState())
            Console.Write(" to ")
            Console.Write(e.NewState())
            Console.WriteLine(".")
            
        End Sub
    
    End Class

Register the state change handler with the FSM as follows:

.. code-block:: vbnet
    
    Dim handler As StateChangeHandler = new StateChangeHandler()
    Dim appInstance As AppClass = new AppClass()
    
    appInstance.addStateChangeHandler(AddressOf handler.StateChange)
