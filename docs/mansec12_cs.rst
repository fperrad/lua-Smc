C# Sample
---------

.Net events required listeners to register an event directly
with the event-raising object.
For SMC, the finite state machine class ``FSMContext`` does the event raising.
But the context class should be kept private within the application class.
The following code shows how to add
and remove state change listeners indirectly,
leaving the FSM inaccessible.

The ``StateChangeEventArgs`` data methods are:

  - ``FSMName()``: returns the FSM's name as a string.
  - ``TransitionType()``: returns one of the following string's: ``SET``, ``PUSH`` or ``POP``.
  - ``PreviousState()``: returns the state which the FSM exited.
  - ``NewState()``: returns the state which at the FSM entered.

.. note::
    
    Because applications use multiple state machines
    and a state change handler may register with multiple FSMs,
    I have added the property ``FSMContext.Name``.
    This name is placed into StateChangeEventArgs
    to allow ready identification of which FSM changed state.
    The default state name is "FSMContext".

.. code-block:: c#
    
    use System;
    use statemap;
    
    public class AppClass
    {
        ...
        
        public void AddStateChangeHandler(StateChangeEventHandler handler)
        {
            _fsm.StateChange += handler;
            return;
        }
        
        public void RemoveStateChangeHandler(StateChangeEventHandler handler)
        {
            _fsm.StateChange -= handler;
            return;
        }
    }

If a class wishes to receive state change events,
then it must implement a method with the following signature.

.. note::
    
    the method does not have to be named ``StateChanged``.

.. code-block:: c#
    
    use System;
    use statemap;
    
    public class StateChangeHandler
    {
        public void StateChanged(object sender, StateChangeEventArgs args)
        {
            //Handle the state change event.
            Console.WriteLine("FSM " +
                              args.FSMName() +
                              " " +
                              args.TransitionType() +
                              " transition from " +
                              args.PreviousState() +
                              " to " +
                              args.NewState() +
                              ".");
            return;
        }
    }

Register the state change handler instance with the FSM as follows:

.. code-block:: c#
    
    StateChangeHandler handler = new StateChangeHandler();
    AppClass appInstance = new AppClass();
    
    appInstance.AddStateChangeHandler(new StateChangeEventHandler(handler.StateChange));
