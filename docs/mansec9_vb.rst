VB.net
------

SMC makes full use of .net's object serialization.
Assuming that ``AppClass`` has the ``<Serializable()>`` attribute
and that the ``_fsm`` member data is not marked as ``<NonSerializable()>``,
then serializing the ``AppClass`` instance will also serialize the FSM:

SMC considers the code it generates to be subservient to the application code.
For this reason the SMC code does not serialize its references to
the FSM context owner or property listeners.
The application code after deserializing the FSM *must* call
the ``Owner`` property setter to re-establish the application/FSM link.
If the application listens for FSM state transitions,
then event handlers must also be put back in place.

.. code-block:: vbnet

    Imports System
    Imports System.IO
    Imports System.Runtime.Serialization
    Imports System.Runtime.Serialization.Formatters.Binary
    
    <Serializable()> Public Class AppClass
        Implements IDeserializationCallback
        
        Private _fsm As AppClassContext
        
        Public Sub New ()
            
            _fsm = New AppClassContext(Me)
        End Sub
        
        ' Restore the FSM context's reference to this object.
        ' This is necessary because AppClass and AppClassContext
        ' reference each other. When AppClass is serialized, its
        ' AppClassContext instance is serialized. But when
        ' AppClassContext is serialized, its AppClass instance is
        ' not serialized which breaks the circual reference. When
        ' AppClassContext is deserialized, its AppClass reference
        ' is Nothing. Hence the need to implement IDeserialization
        ' and reset AppClassContext's AppClass reference.
        ' Event handlers must also be put back in place for the same
        ' reason.
        Private Sub OnDeserialization(ByVal send As Object) _
            Implements IDeserializationCallback.OnDeserialization
            
            _fsm.Owner = Me
            AddHandler _fsm.StateChange, handler
        End Sub
        
        Shared Sub Main()
            
            Serialize()
            Deserialize()
        End Sub
        
        Shared Sub Serialize()
            
            Dim appInstance As New AppClass()
            Dim stream As Stream = _
                File.Open("fsm_serial.bin", FileMode.Create)
            Dim formatter As New BinaryFormatter()
            
            Try
                
                formatter.Serialize(stream, appInstance)
            Catch serialex As SerializationException
            
                ' Handle serialization failure.
            Finally
                
                stream.Close()
            End Try
        End Sub
        
        ...
        
    End Class

Recreating the persisted ``AppClass`` instance is equally simple:

.. code-block:: vbnet

    Shared Sub Deserialize()
        
        Dim appInstance As AppClass = Nothing
        Dim stream As Stream = _
            File.Open("fsm_serial.bin", FileMode.Open)
        Dim formatter As New BinaryFormatter()
        
        Try
            
            appInstance = _
                CType(formatter.Deserialize(stream), AppClass)
        Catch serialex As SerializationException
            
            ' Handle deserialization failure.
        Finally
            
            stream.Close()
        End Try
    End Sub
