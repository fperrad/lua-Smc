Giving Direction
----------------

The SMC syntax provides a number of ``%`` directives.
Each directive's use is described in this section.

  - **%start** Specifies the state machine start state in the form ``<map>::<state>``
    
    Required: Yes.
    
    Supported target languages: All.

  - **%class** Specifies the associated state machine context class providing the guard and action methods.
    
    Required: Yes.
    
    Supported target languages: All.
    
  - **%fsmclass** Specifies the generated state machine class name.
    
    Required: No.
    
    Default settings: ``<%class name>Context``
    
    Supported target languages: All.
    
  - **%package** Put the generated code into this package/namespace.
    
    Required: No.
    
    Default settings: None.
    
    Supported target languages: All.
    
  - **%include** Used to a include header file needed by the generated state machine code.
    
    Required: No.
    
    Default settings: No includes.
    
    Supported target languages: C, C++, Objective-C
    
  - **%import** Used to import/use a class needed by the generated state machine code.
    
    Required: No.
    
    Default settings: No imports.
    
    Supported target languages: C#, C++, Groovy, JavaScript, Java, Lua,
    Perl, PHP, Python, Ruby, Scala, TCL, VB.NET
    
  - **%declare** C/C++/Objective-C forward declaration placed in the header file.
    
    Required: No.
    
    Default settings: No declarations.
    
    Supported target languages: C, C++, Objective-C
    
  - **%access** Specifies the generated class' accessibility level.
    
    Required: No.
    
    Default settings: None.
    
    Supported target languages: C#
    
  - **%map** SMC states are grouped into maps.
    The ``%map`` followed by a ``%%`` pair groups the states appearing within.
