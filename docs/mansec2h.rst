Transition Arguments
--------------------

.. image:: ./Images/TransArg.png

.. code-block:: sm

    // State
    Idle
    {
        // Trans
        Run(msg: const Message&)
        
          // Guard condition
          [ctxt.isProcessorAvailable() == true &&
           ctxt.getConnection().isOpen() == true]
        
            // Next State
            Running
            // Actions
            {
                StopTimer("Idle");
                DoWork(msg);
            }
        
        Run(msg: const Message&)
            // Next State       Actions
            nil                 {RejectRequest(msg);}
    }

Note: When using transition guards and transition arguments,
multiple instances of the same transition must have the same argument list.
Just as with C++ and Java methods, 
the transitions ``Run(msg: const Message&)`` and ``Run()`` are *not* the same transition.
Failure to use the identical argument list when defining the same transition
with multiple guards will result in incorrect code being generated.

.. rubric:: Tcl "arguments":

While Tcl is a type-less language, Tcl does distinguish between call-by-value and call-by-reference.
By default SMC will generate call-by-value Tcl code if the transition argument has no specified type.
But you may use the artificial types "value" or "reference".

If your Tcl-targeted FSM has a transition:

.. code-block:: sm

    DoWork(task: value)
        Working
        {
            workOn(task); 
        }

then the generated Tcl is:

.. code-block:: tcl

    public method DoWork {task} {
        workOn $this $task;
    }

If your Tcl-targeted FSM has a transition:

.. code-block:: sm

    DoWork(task: reference)
        Working
        {
            workOn(task);
        }

then the generated Tcl is:

.. code-block:: tcl

    public method DoWork {task} {
        workOn $this task;
    }

The method ``workOn`` must ``upvar`` the task parameter:

.. code-block:: tcl

    public method workOn {taskName} {
        upvar $taskName task;
        ...
    }

.. rubric:: Lua/Python/Ruby "arguments":

While Lua/Python/Ruby is a dynamically typed language and does not use types for function parameter definitions,
you could provide a optional data type for transition arguments.
This "data type" is ignored when generating the target Lua/Python/Ruby code.
I suggest using meaningful type names.

.. code-block:: sm

    DoWork(task: TaskObj, runtime: Ticks)
        Working
        {
            ...
        }

.. rubric:: Groovy/PHP "arguments":

While Groovy/PHP gives the choice between static and dynamic typing,
you could provide a optional data type for transition arguments.
In this case, the type is used when generating the target Groovy/PHP code.

The PHP variable syntax is like Perl (named with '$').

.. rubric:: Perl "arguments":

While Perl is a dynamically typed language and does not use types for function parameter definitions,
you could provide a optional data type for transition arguments.
This "data type" is ignored when generating the target Perl code.
I suggest using meaningful type names.

Only Perl scalar values (ie, named with '$') are allowed.

.. code-block:: sm

    DoWork($task: TaskObj, $runtime: Ticks)
        Working
        {
            ...
        }
